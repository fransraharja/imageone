//
//  NSString+WebService.m
//  ImageOne
//
//  Created by Vensi Developer on 11/25/13.
//
//

#import "NSString+WebService.h"

@implementation NSString (WebService)



-(NSString*)URLEncode
{
    NSString *newString = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)self, NULL, (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
    return newString;
}
-(id)JSON
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves|NSJSONReadingMutableContainers error:nil];
}

@end
