//
//  NSString+WebService.h
//  ImageOne
//
//  Created by Vensi Developer on 11/25/13.
//
//

#import <Foundation/Foundation.h>

@interface NSString (WebService)

-(NSString*) URLEncode;

-(id)JSON;

@end
