//
//  IOSRequest.m
//  ImageOne
//
//  Created by Vensi Developer on 11/25/13.
//
//

#import "IOSRequest.h"

@implementation IOSRequest


+(void)requestToPath:(NSString *)path onCompletion:(RequestCompletionHandler)complete;
{
    NSOperationQueue *backgroundQueue = [[NSOperationQueue alloc]init];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:nil cachePolicy:NSURLCacheStorageAllowedInMemoryOnly timeoutInterval:10];
    
    [NSURLConnection sendAsynchronousRequest:request queue:backgroundQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {

        NSString *result = [[NSString alloc]initWithData:data encoding: NSUTF8StringEncoding];
        if(complete)
        {
            complete(result,error);
        }
    }];
}

+(void) loginWithUsername: (NSString *)userName
              andPassword: (NSString *)password
             oncompletion:(RequestDictionaryCompletionHandler)complete
{
    userName = [password URLEncode];
    password = [password URLEncode];
    
    NSString *basePath = @"http://imageone.vensi.co/webservices/authentication.php";
    NSString *fullPath = [basePath stringByAppendingFormat:@"uname:%@&pswd:%@" ,userName,password];
    [IOSRequest requestToPath:fullPath onCompletion:^(NSString *result,NSError *error)
     {
         if(error || [result isEqualToString:@""])
         {
             if(complete)
             {
               complete(nil);
             }
         }
         else
         {
             NSDictionary *user = [result JSON];
             if(complete)
                 complete(user);
         }
     }];
}

@end
