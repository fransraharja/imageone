//
//  LoginViewController.h
//  ImageOne
//
//  Created by Vensi Developer on 11/12/13.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ViewController.h"

@interface LoginViewController : UIViewController<UITextViewDelegate,UINavigationControllerDelegate,NSURLConnectionDelegate>
{
    NSMutableData *_reponseData;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *usernameLabel;

@property (strong, nonatomic) IBOutlet UITextField *passwordLabel;

@property (strong, nonatomic) UIActivityIndicatorView *loading;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IBOutlet UIButton *login;

@property (retain,nonatomic) NSString *address;

-(void)prepareData:(NSString *)address;

@end
