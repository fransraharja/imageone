//
//  TableViewPopOver.h
//  ImageOne
//
//  Created by Vensi Developer on 8/15/13.
//
//

#import <UIKit/UIKit.h>

@protocol TableViewPopOverDelegate <NSObject>

@required
-(void)selectedRow:(NSInteger)index;
@end

@interface TableViewPopOver : UITableViewController

@property (nonatomic, strong) NSMutableArray *list;
@property (nonatomic, copy) NSString *sectionTitle;
@property (nonatomic, unsafe_unretained) id<TableViewPopOverDelegate> delegate;

@end
