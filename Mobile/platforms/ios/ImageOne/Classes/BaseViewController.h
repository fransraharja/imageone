//
//  LogoutSuperClass.h
//  
//
//  Created by Vensi Developer on 11/18/13.
//
//

#import <UIKit/UIKit.h>
#import <Cordova/CDVViewController.h>
#import <Cordova/CDVCommandDelegateImpl.h>
#import <Cordova/CDVCommandQueue.h>

@interface BaseViewController : CDVViewController

@property (retain,strong)UIView *topNavImgView;
@property(retain,strong) UIButton *logoutBtn;
@property (retain,strong) NSString *usernameAddr;
@property (retain,strong) NSString *passwordAddr;
@property (retain,nonatomic) NSDictionary *authDetails;
- (void)createButton;

@end
