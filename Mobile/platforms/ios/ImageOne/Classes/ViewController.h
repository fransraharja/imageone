//
//  ViewController.h
//  ImageOne
//
//  Created by RAMANUJAN SAMPATHKUMARAN on 2/20/12.
//  Copyright (c) 2012 Saint Xvier University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "ProposalViewController.h"
#import "TableViewPopOver.h"
#import "MainViewController.h"
#import "BaseViewController.h"

@class PVCDelegate;

@interface ViewController : BaseViewController<MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,TableViewPopOverDelegate,PVCDelegate>
{
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *versionNo;

    NSMutableArray *list,*tbReason;
    UIButton *turnBack,*submit,*selected,*submitBtn,*turnBackBtn,*deleteBtn;
 
    MFMailComposeViewController *submitController,*turnBackController;
}

@property (nonatomic,retain) TableViewPopOver *tableViewPopOver;
@property(nonatomic, strong) UIPopoverController *popOverController;

@property(nonatomic,retain) IBOutlet UITableView *tableView;
@property(nonatomic, retain) NSMutableDictionary *identifier;
@property(nonatomic, retain) NSMutableArray *list;

- (IBAction)startNewProposal:(id)sender;

- (IBAction)profile:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *profileBtn;

-(void)sendEmail;

@end