//
//  FormViewController.m
//  
//
//  Created by Vensi Developer on 11/10/10.
//  Copyright 2010 Vensi, Inc. All rights reserved.
//

#import "FormViewController.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 264;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 352;

@implementation FormViewController

@synthesize nxtPrevDoneBar,segControl, doneButton, activeField, entryFields;

- (void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
	
	// Keyboard notifications
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillShow:) 
												 name:UITextViewTextDidBeginEditingNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(keyboardWillHide:) 
												 name:UITextViewTextDidEndEditingNotification object:nil];
	
	if (!entryFields) {  
		self.entryFields = [[NSMutableArray alloc] initWithArray:  
							[self entryFieldsInView:self.view]];
		CGRect viewFrame = self.view.frame;
		resetY = viewFrame.origin.y;
	}
}  

- (void)viewWillDisappear:(BOOL)animated {
	
	[super viewWillDisappear:animated];
	
	[nxtPrevDoneBar removeFromSuperview];
	
	[self selDoneField:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark Keyboard Notifications
- (void)keyboardWillShow:(NSNotification *)notification {
	UITextView *textView = (UITextView *)[notification valueForKey:@"object"];
	activeField = textView;
	//[self scrollActiveFieldToVisible];
		
}

- (void)keyboardWillHide:(NSNotification *)notification {
	//[self scrollBackView];
	
}

#pragma mark -
#pragma mark TextField Delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	return [textField resignFirstResponder];	
	//return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField.tag == 11000 || textField.tag == 12000 || textField.tag == 13000 || textField.tag == 15000 || textField.tag == 999){
        textField.returnKeyType = UIReturnKeyDone;
        return;
    }

	if(nxtPrevDoneBar == nil) {
		nxtPrevDoneBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
		nxtPrevDoneBar.barStyle = UIBarStyleBlackTranslucent;
		
		segControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Previous",@"Next",nil]];
		segControl.frame = CGRectMake(10, 6, 130,30);
		segControl.segmentedControlStyle = UISegmentedControlStyleBar;
		segControl.momentary = YES;
		segControl.tintColor = [UIColor blackColor];
        
		[segControl addTarget:self action:@selector(nextPrevious:) forControlEvents:UIControlEventValueChanged];     
		
		if ( !doneButton )
			doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selDoneField:)];
		
		UIBarButtonItem *btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
		[nxtPrevDoneBar setItems:[NSArray arrayWithObjects: btnSpace, doneButton, nil] animated:YES];
		[nxtPrevDoneBar addSubview:segControl];
	}
	
	[textField setInputAccessoryView:nxtPrevDoneBar];
	
	//[activeField resignFirstResponder];
	activeField = textField;
	//[self scrollActiveFieldToVisible];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	activeField = nil;
	//[self scrollBackView];
}

- (BOOL)textField: (UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {    
    
	return YES;
}


#pragma mark -
#pragma mark TextView Delegate Methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	if(textView.tag == 12000){
        textView.returnKeyType = UIReturnKeyDone;
        return YES;
    }
    
	if(nxtPrevDoneBar == nil) {
		nxtPrevDoneBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
		nxtPrevDoneBar.barStyle = UIBarStyleBlackTranslucent;
        
		segControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Previous",@"Next",nil]];
		segControl.frame = CGRectMake(10, 6, 130,30);
		segControl.segmentedControlStyle = UISegmentedControlStyleBar;
		segControl.momentary = YES;
		segControl.tintColor = [UIColor blackColor];
        
		[segControl addTarget:self action:@selector(nextPrevious:) forControlEvents:UIControlEventValueChanged];     
        
		if ( !doneButton )
			doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(selDoneField:)];
        
		UIBarButtonItem *btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
		[nxtPrevDoneBar setItems:[NSArray arrayWithObjects: btnSpace, doneButton, nil] animated:YES];
		[nxtPrevDoneBar addSubview:segControl];
	}
    
	[textView setInputAccessoryView:nxtPrevDoneBar];
    
	return YES;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(textView.tag == 12000 && [text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
	return YES;
}

#pragma mark -
#pragma mark Button Action Methods
-(void) nextPrevious:(id)sender {
    UISegmentedControl *segContr = (UISegmentedControl *)sender;
    for (int i=0; i<[segContr.subviews count]; i++)
    {
        if ([[segContr.subviews objectAtIndex:i]isSelected] )
        {
            UIColor *tintcolor=[UIColor colorWithRed:0.6 green:0.2 blue:0.8 alpha:0.2];
            [[segContr.subviews objectAtIndex:i] setTintColor:tintcolor];
        }
        else
        {
            [[segContr.subviews objectAtIndex:i] setTintColor:[UIColor blackColor]];
        }
    }
	switch([(UISegmentedControl *)sender selectedSegmentIndex]) {
		case 0:
		{
			NSUInteger prevIndex = [entryFields indexOfObject:activeField] - 1;
			if ( prevIndex == -1 )
				prevIndex = [entryFields count] - 1;
			
			if ( [entryFields count] < prevIndex ) {  
				[activeField resignFirstResponder];
			} else {
				[activeField resignFirstResponder];
				[[entryFields objectAtIndex:prevIndex] becomeFirstResponder];
			}			
		}
			break;
		case 1:
		{
			NSUInteger nextIndex = [entryFields indexOfObject:activeField] + 1;
			if ( nextIndex == [entryFields count] )
				nextIndex = 0;
			
			if ( [entryFields count] < nextIndex) {  
				[activeField resignFirstResponder];
			} else {
				[activeField resignFirstResponder];
				[[entryFields objectAtIndex:nextIndex] becomeFirstResponder];
			}
		}
			break;
			
		default:
			break;
	}
}

-(void) selDoneField:(id)sender {
	[activeField resignFirstResponder];
}

#pragma mark -
#pragma mark Scrolling helper Methods
- (NSArray *) entryFieldsInView:(UIView *)tView {
	
	NSMutableArray *myArray = [[NSMutableArray alloc] init]; 
	
	for(int i = 0; i < [tView.subviews count]; i++)
    {
		UIView *aView  = [tView.subviews objectAtIndex:i];
		
            if([aView isKindOfClass:[UITextField class]]) { 
                if(((UITextField *)aView).tag == 11000 || ((UITextField *)aView).tag == 12000 || ((UITextField *)aView).tag == 999)
                    return myArray;
			if(((UITextField *)aView).delegate==nil)
				((UITextField *)aView).delegate=self;
			[myArray addObject:aView]; 
		} else if ( [aView isKindOfClass:[UITextView class]]) { 
            if(((UITextView *)aView).tag == 12000)
                return myArray;

			if(((UITextView *)aView).delegate==nil)
				((UITextView *)aView).delegate=self;
			[myArray addObject:aView]; 
		} else if ( [aView isKindOfClass:[UITableViewCell class]]) { 
			[myArray addObjectsFromArray:[self entryFieldsInView:aView]];
		} else if ( [aView isKindOfClass:[UITableView class]]) { 
			UITableView *tTablV = (UITableView *)aView;
			NSArray *tblCells = [tTablV visibleCells];
			for( int j=0; j < [tblCells count]; j++ ) {
				[myArray addObjectsFromArray:[self entryFieldsInView:[tblCells objectAtIndex:j]]];
			}
		} else if ( [aView.subviews count] > 0 ) {
			[myArray addObjectsFromArray:[self entryFieldsInView:aView]];
		}
	}
	
    return myArray;
}


@end

