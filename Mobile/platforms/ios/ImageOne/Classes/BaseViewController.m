//
//  LogoutSuperClass.m
//  
//
//  Created by Vensi Developer on 11/18/13.
//
//

#import "BaseViewController.h"

@implementation BaseViewController

-(void)viewDidLoad
{
    [self createButton];
    [super viewDidLoad];
}

- (void)createButton {
    
    UIImage *image= [UIImage imageNamed: @"TOPNAV_BAR.png"];
    _topNavImgView = [[UIImageView alloc]initWithImage:image];
    [_topNavImgView setCenter:CGPointMake(self.view.frame.size.width/2, 50)];
    [_topNavImgView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin];
    
    _logoutBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _logoutBtn.frame = CGRectMake(690, 40, 68, 31); // position in the parent view and set the size of the button
    [_logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [_logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_logoutBtn addTarget:self action:@selector(Logout:) forControlEvents:UIControlEventTouchDown];
    [_logoutBtn setBackgroundColor:[UIColor clearColor]];
    [_topNavImgView setUserInteractionEnabled:YES];
    [_topNavImgView addSubview: _logoutBtn];
    [self.view addSubview:_topNavImgView];
    [self.view bringSubviewToFront:_logoutBtn];
        //[self.view addSubview:_logoutBtn];
}

- (IBAction)Logout:(UIButton *)sender
{
    NSString *secretKey = [_authDetails valueForKey:@"secretKey"];
    NSString *url = [NSString stringWithFormat:@"http://imageone.vensi.co/webservices/logout.php"];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    NSString *strdata=[[NSString alloc]initWithData:data
    encoding:NSUTF8StringEncoding];
    
    if(strdata == secretKey)
    {
    UIAlertView *logoutAlert = [[UIAlertView alloc] initWithTitle:@"Logout" message:@"Are you sure want to logout?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    [logoutAlert show];
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Yes"])
    {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


@end
