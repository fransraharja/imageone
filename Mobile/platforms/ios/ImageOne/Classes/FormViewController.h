//
//  FormViewController.h
//  RDC
//
//  Created by Vensi Developer on 11/10/10.
//  Copyright 2010 Vensi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface FormViewController : BaseViewController <UITextFieldDelegate, UITextViewDelegate> {
	
	UIToolbar *nxtPrevDoneBar;
	
	UISegmentedControl *segControl;
	UIBarButtonItem *doneButton;
	
	NSMutableArray *entryFields;
	UIView *activeField;
	
	CGFloat animatedDistance;
	CGFloat resetY;
}

@property (nonatomic, retain) UIToolbar *nxtPrevDoneBar;
@property (nonatomic, retain) UISegmentedControl *segControl;
@property (nonatomic, retain) UIBarButtonItem *doneButton;

@property (nonatomic, retain) UIView *activeField;

@property (nonatomic, retain) NSMutableArray *entryFields;

-(void) selDoneField:(id)sender;

- (NSArray *) entryFieldsInView:(UIView *)tView;
//- (void) scrollActiveFieldToVisible;
//- (void) scrollBackView;

@end
