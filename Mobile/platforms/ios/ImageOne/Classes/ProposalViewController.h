//
//  ProposalViewController.h
//  ImageOne
//
//  Created by RAMANUJAN SAMPATHKUMARAN on 2/23/12.
//  Copyright (c) 2012 Saint Xvier University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormViewController.h"
#import "TableViewPopOver.h"
#import <QuartzCore/QuartzCore.h>
#import "BaseViewController.h"

@protocol PVCDelegate

@optional
-(void)reloadTable;
@end

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ProposalViewController : FormViewController<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TableViewPopOverDelegate>

{
    id<PVCDelegate> delegate;
    
    UIScrollView*scroller,*faciltyScroller,*kitchenScroller,*bathroomScroller;
    NSMutableArray *list;
    NSData *imgData;
    NSDateFormatter *dateFormat;
    NSString *turnBack,*turnBackSent,*submitted;
    int i,th,lh,lw,cblw,selectedFaci;
    
    UIColor *teal;
    UILabel *calcLabel;
    UIDatePicker *dobPicker;
    UIButton *TbAbutton,*addButton1,*addButton2,*addButton3,*delButton1,*delButton2,*delButton3,*facilitybtn,*kitchenbtn,*roomTypebtn,
    *floorTypebtn,*Tba,*selectedBtn;
    int cellIndex,y,z,v,j,k,l;
}

@property (nonatomic,retain) id<PVCDelegate> delegate;
@property (nonatomic,retain) TableViewPopOver *tableViewPopOver;
@property(nonatomic, strong) UIPopoverController *popOverController,*datePopOverController;

@property (nonatomic, readwrite) int cellIndex;
@property (nonatomic, retain)NSMutableArray *list;
@property (nonatomic,retain)IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIButton *homeBtn;
@property (nonatomic,retain)IBOutlet UIView *haveKeys,*dayPorter,*trashLiners,*bthConsumables,*whichDays,*specialAreas;
@property (nonatomic, retain)IBOutlet UIButton *billDifferent,*visitors,*ReasonBtn,*submitBtn,*Appointment,*bidDeliveryDate,*rating,*tillWhen;
@property (nonatomic, retain)IBOutlet UITextField *email,*identifier,*company,*contact,*phoneNumber,*address,*city,*zip,*productionRate,*salesRep,*leadSource,*TbanoofE,*manHours,*hourlyRate,*ic,*multiprice2,*multiprice1,*adjustedPrice,*cptC,*vctC,*cerC,*vsgC,*crtC,*linC,*woodC,*trzC,*rubC,*epoxyC,*curService,*cost,*howManyPeople,*howMuchTime,*underContract,*decisionBeMade,*byWho,*perWeek,*restrictedHours,*priceOfItems,*hours,*hourRate,*sink,*micro,*refrig,*vending,*tables,*water,*dish,*costOfSpecial;
@property(nonatomic,retain)IBOutlet UITextView *notes,*note,*specialItems;
@property (nonatomic,retain)IBOutlet UILabel *price,*frequency,*dailyCost,*adjustedDailyCost,*totalSquareFeet,*cptTS,*cptTC,*vctTS,*vctTC,*cerTS,*cerTC,*vsgTS,*vsgTC,*crtTS,*crtTC,*linTS,*linTC,*woodTS,*woodTC,*trzTS,*trzTC,*rubTS,*rubTC,*epoxyTS,*epoxyTC,*totalFixtures;

- (IBAction)submitBtnPressed:(id)sender;
- (IBAction)calBtnPressed:(id)sender;
-(IBAction)TableAlertButton:(UIButton *)sender;
-(IBAction)datePickerBtn:(id)sender;
//-(IBAction)valueChanged:(id)sender;
- (IBAction)radioButton:(id)sender;
-(IBAction)checkBoxButton:(id)sender;
-(IBAction)scrollToBottom:(id)sender;
-(IBAction)camera:(id)sender;
-(IBAction)backBtnClicked:(id)sender;

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;

-(void)addButton1:(NSMutableDictionary *)dict;
-(void)addButton2:(NSMutableDictionary *)dict;
-(void)addButton3:(NSMutableDictionary *)dict;
-(void)totSqfeet:(UIView*)vw;
-(void)calculateBtn;
-(void)saveData;
-(void)calcMonthlyQuote;

@end

