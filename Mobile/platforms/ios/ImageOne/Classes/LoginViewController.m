//
//  LoginViewController.m
//  ImageOne
//
//  Created by Vensi Developer on 11/12/13.
//
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define loginURL [NSURL URLWithString:@"http://imageone.vensi.co/webservices/authentication.php"] //1
#define logoutURL [NSURL URLWithString:@"http://imageone.vensi.co/webservices/logout.php"] //2


#import "LoginViewController.h"
#define kOFFSET_FOR_KEYBOARD 80.0
@interface NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:
(NSString*)urlAddress;
-(NSData*)toJSON;

@end

@implementation NSDictionary(JSONCategories)+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress
{
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

-(NSData*)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end
@implementation LoginViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)prepareData:(NSString *)address
{
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:
                        loginURL];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
    });
}

- (void)fetchedData:(NSMutableData *)responseData {
    
    NSString *links = [NSString stringWithFormat:@"%@?uname=%@&pswd=%@",loginURL,self.usernameLabel.text,self.passwordLabel.text];
    NSMutableURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:links] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
    
//    [request setHTTPMethod:@"POST"];
//    [request setValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    NSString *stringData = @"{'uname':username, 'pswd': password}";
//    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
//    request.HTTPBody = requestBodyData;
    [NSURLConnection connectionWithRequest:request delegate:self];
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _reponseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_reponseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:_reponseData //1
                          
                          options:kNilOptions
                          error:&error];

    NSString *secretKey = [json objectForKey:@"secrectKey"];
    
    if(secretKey)
    {
        UIAlertView *loginsuccess = [[UIAlertView alloc]initWithTitle:@"Directing.." message:@"Login Successful" delegate:self cancelButtonTitle:@"OK!" otherButtonTitles:nil];
        [loginsuccess show];
        ViewController *nextViewController= [[ViewController alloc ]initWithNibName:@"ViewController" bundle:nil];
        [self.navigationController pushViewController:nextViewController animated:YES];
        
    }
    else
    {
        UIAlertView *loginfailed = [[UIAlertView alloc]initWithTitle:@"FAILED!" message:@"Login Failed" delegate:self cancelButtonTitle:@"OK!" otherButtonTitles:nil];
        [loginfailed show];
        
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

- (BOOL)validateName:(NSString *) name
{
    NSString *nameRegex = @"[A-Za-z]{1,14}";
    NSPredicate *checkName = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [checkName evaluateWithObject:name];
}

- (BOOL)validatepassword:(NSString *) name
{
    NSString *passRegex = @"[A-Za-z0-9]{1,14}";
    NSPredicate *checkPass = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passRegex];
    return [checkPass evaluateWithObject:name];
}

- (IBAction)loginButton:(id)sender
{
    //to do check if username and password is entered
//    if(([self validateName:self.usernameLabel.text] == false)||([self validatepassword:self.passwordLabel.text] == false))
//    {
//        UIAlertView *userpassalert = [[UIAlertView alloc] initWithTitle:@"Username or password is invalid format or empty"  message:@"Invalid" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [userpassalert show];
//

        //[self performSelectorOnMainThread:@selector(prepareData:)
                        //       withObject:self.address waitUntilDone:YES];
//    }
    
    [self prepareData:_address];
    NSLog(@"Log In button was pressed!");
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.scrollView setScrollEnabled:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(self.usernameLabel == textField)
    {
        [self.passwordLabel becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
