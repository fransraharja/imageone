//
//  ProposalViewController.m
//  ImageOne
//
//  Created by RAMANUJAN SAMPATHKUMARAN on 2/23/12.
//  Copyright (c) 2012 Saint Xvier University. All rights reserved.
//
#import "AppDelegate.h"
#import "ProposalViewController.h"

@implementation ProposalViewController

@synthesize delegate;
@synthesize haveKeys,dayPorter,trashLiners,bthConsumables,whichDays,specialAreas;
@synthesize cellIndex,list,scroller,TbanoofE;
@synthesize email,identifier,company,contact,phoneNumber,address,city,zip,productionRate,salesRep,leadSource,frequency,visitors,price,Appointment,manHours,hourlyRate,totalSquareFeet,ic,adjustedPrice,bidDeliveryDate,multiprice2,multiprice1,note,ReasonBtn,rating,tillWhen,costOfSpecial;
@synthesize cptC,vctC,cerC,vsgC,crtC,linC,woodC,trzC,rubC,epoxyC,totalFixtures;
@synthesize cptTS,cptTC,vctTS,vctTC,cerTS,cerTC,vsgTS,vsgTC,crtTS,crtTC,linTS,linTC,woodTS,woodTC,trzTS,trzTC,rubTS,rubTC,epoxyTS,epoxyTC,curService,cost,howManyPeople,howMuchTime,underContract,decisionBeMade,byWho,perWeek,restrictedHours,priceOfItems,notes,hours,hourRate,sink,micro,refrig,vending,tables,water,dish,specialItems,dailyCost,adjustedDailyCost;
@synthesize submitBtn,billDifferent;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableViewPopOver = [[TableViewPopOver alloc] initWithStyle:UITableViewStylePlain];
    self.tableViewPopOver.delegate = self;
    
    teal = UIColorFromRGB(0x008080);

    self.navigationItem.hidesBackButton = TRUE;
    
    cptC.frame = CGRectMake(576,89,70,18);
    vctC.frame = CGRectMake(576,111,70,18);
    cerC.frame = CGRectMake(576,133,70,18);
    vsgC.frame = CGRectMake(576,155,70,18);
    crtC.frame = CGRectMake(576,177,70,18);
    linC.frame = CGRectMake(576,199,70,18);
    woodC.frame = CGRectMake(576,221,70,18);
    trzC.frame = CGRectMake(576,243,70,18);
    rubC.frame = CGRectMake(576,265,70,18);
    epoxyC.frame = CGRectMake(576,287,70,18);
    [[note layer] setCornerRadius:5.0];
    [[notes layer] setCornerRadius:5.0];
    [[specialItems layer] setCornerRadius:5.0];
    
    [scroller setScrollEnabled:YES];
    [scroller setContentSize:CGSizeMake(768, 3850)];
    [visitors setTitle:@"Select" forState:UIControlStateNormal];
    [ReasonBtn setTitle:@"Select" forState:UIControlStateNormal];
    [self.view bringSubviewToFront:self.homeBtn];
    [self performSelector:@selector(loadData) withObject:nil afterDelay:0.05];
}

-(void)loadData{
    addButton1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [addButton1 setFrame:CGRectMake(650,1995, 80, 35)];
    [addButton1 setTitle:@"Add" forState:UIControlStateNormal];
    [addButton1 addTarget:self action:@selector(addButton1:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:addButton1];

    UIButton *dupButton1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [dupButton1 setFrame:CGRectMake(550,1995, 80, 35)];
    dupButton1.tag = 1;
    [dupButton1 setTitle:@"Duplicate" forState:UIControlStateNormal];
    [dupButton1 addTarget:self action:@selector(dupButton:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:dupButton1];

    UILabel *facility = [[UILabel alloc] initWithFrame:CGRectMake(30,1995,150,35)];
	facility.textColor = [UIColor greenColor];
	facility.backgroundColor = [UIColor clearColor];
	facility.font = [UIFont systemFontOfSize:18];
    facility.text=@"Facility Type:";
	[scroller addSubview:facility];
    
    facilitybtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [facilitybtn setFrame:CGRectMake(190,1995, 160, 35)];
    facilitybtn.tag=9998;
    [facilitybtn setTitle:@"Select" forState:UIControlStateNormal];
    [facilitybtn addTarget:self action:@selector(TableAlertButton:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:facilitybtn];
    
    UILabel *officeSize = [[UILabel alloc] initWithFrame:CGRectMake(50,2045,150,35)];
	officeSize.textColor = [UIColor greenColor];
	officeSize.backgroundColor = [UIColor clearColor];
	officeSize.font = [UIFont systemFontOfSize:18];
    officeSize.text=@"Office Size";
	[scroller addSubview:officeSize];
    
    UILabel *Type = [[UILabel alloc] initWithFrame:CGRectMake(205,2045,130,35)];
	Type.textColor = [UIColor greenColor];
	Type.backgroundColor = [UIColor clearColor];
	Type.font = [UIFont systemFontOfSize:18];
    Type.text=@"Room Type";
	[scroller addSubview:Type];
    
    UILabel *Flooring = [[UILabel alloc] initWithFrame:CGRectMake(350,2045,150,35)];
	Flooring.textColor = [UIColor greenColor];
	Flooring.backgroundColor = [UIColor clearColor];
	Flooring.font = [UIFont systemFontOfSize:18];
    Flooring.text=@"Flooring Type";
	[scroller addSubview:Flooring];
    
    UILabel *Tot = [[UILabel alloc] initWithFrame:CGRectMake(515,2045,70,35)];
	Tot.textColor = [UIColor greenColor];
	Tot.backgroundColor = [UIColor clearColor];
    Tot.textAlignment = UITextAlignmentCenter;
	Tot.font = [UIFont systemFontOfSize:20];
    Tot.text=@"Notes";
	[scroller addSubview:Tot];
    
    UILabel *TotalSqft = [[UILabel alloc] initWithFrame:CGRectMake(635,2045,90,35)];
	TotalSqft.textColor = [UIColor greenColor];
	TotalSqft.backgroundColor = [UIColor clearColor];
	TotalSqft.font = [UIFont systemFontOfSize:18];
    TotalSqft.text=@"Total Sq ft.";
	[scroller addSubview:TotalSqft];
    
    faciltyScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(5,2095,758,410)];
    [faciltyScroller.layer setBorderWidth:1.5];
    [faciltyScroller.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [faciltyScroller.layer setCornerRadius:5.0f];
    [scroller addSubview:faciltyScroller];
    
    UILabel *kitchen = [[UILabel alloc] initWithFrame:CGRectMake(30,2620,150,35)];
	kitchen.textColor = [UIColor greenColor];
	kitchen.backgroundColor = [UIColor clearColor];
	kitchen.font = [UIFont systemFontOfSize:18];
    kitchen.text=@"Kitchen :";
	[scroller addSubview:kitchen];
    
    UIButton *dupButton2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    dupButton2.tag = 2;
    [dupButton2 setFrame:CGRectMake(550,2620, 80, 35)];
    [dupButton2 setTitle:@"Duplicate" forState:UIControlStateNormal];
    [dupButton2 addTarget:self action:@selector(dupButton:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:dupButton2];

    addButton2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [addButton2 setFrame:CGRectMake(650,2620, 80, 35)];
    [addButton2 setTitle:@"Add" forState:UIControlStateNormal];
    [addButton2 addTarget:self action:@selector(addButton2:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:addButton2];
    
    UILabel *officeSize1 = [[UILabel alloc] initWithFrame:CGRectMake(50,2670,150,35)];
	officeSize1.textColor = [UIColor greenColor];
	officeSize1.backgroundColor = [UIColor clearColor];
	officeSize1.font = [UIFont systemFontOfSize:18];
    officeSize1.text=@"Room Size";
	[scroller addSubview:officeSize1];
    
    UILabel *kType = [[UILabel alloc] initWithFrame:CGRectMake(240,2670,130,35)];
	kType.textColor = [UIColor greenColor];
	kType.backgroundColor = [UIColor clearColor];
	kType.font = [UIFont systemFontOfSize:18];
    kType.text=@"Flooring Type";
	[scroller addSubview:kType];
    
    UILabel *notes1 = [[UILabel alloc] initWithFrame:CGRectMake(420,2670,180,35)];
	notes1.textColor = [UIColor greenColor];
	notes1.backgroundColor = [UIColor clearColor];
    notes1.textAlignment = UITextAlignmentCenter;
	notes1.font = [UIFont systemFontOfSize:18];
    notes1.text=@"Notes";
	[scroller addSubview:notes1];

    UILabel *TotalSqft1 = [[UILabel alloc] initWithFrame:CGRectMake(635,2670,90,35)];
	TotalSqft1.textColor = [UIColor greenColor];
	TotalSqft1.backgroundColor = [UIColor clearColor];
	TotalSqft1.font = [UIFont systemFontOfSize:18];
    TotalSqft1.text=@"Total Sq ft.";
	[scroller addSubview:TotalSqft1];
    
    kitchenScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(5,2720,758,166)];
    [kitchenScroller.layer setBorderWidth:1.5];
    [kitchenScroller.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [kitchenScroller.layer setCornerRadius:5.0f];
    [scroller addSubview:kitchenScroller];
    
    UILabel *bathroom = [[UILabel alloc] initWithFrame:CGRectMake(30,2910,150,35)];
	bathroom.textColor = [UIColor greenColor];
	bathroom.backgroundColor = [UIColor clearColor];
	bathroom.font = [UIFont systemFontOfSize:18];
    bathroom.text=@"Bathroom :";
	[scroller addSubview:bathroom];
    
    addButton3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [addButton3 setFrame:CGRectMake(650,2910, 80, 35)];
    [addButton3 setTitle:@"Add" forState:UIControlStateNormal];
    [addButton3 addTarget:self action:@selector(addButton3:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:addButton3];
    
    UIButton *dupButton3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [dupButton3 setFrame:CGRectMake(550,2910, 80, 35)];
    dupButton3.tag = 3;
    [dupButton3 setTitle:@"Duplicate" forState:UIControlStateNormal];
    [dupButton3 addTarget:self action:@selector(dupButton:) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:dupButton3];

    UILabel *floorSize = [[UILabel alloc] initWithFrame:CGRectMake(50,2960,150,35)];
	floorSize.textColor = [UIColor greenColor];
	floorSize.backgroundColor = [UIColor clearColor];
	floorSize.font = [UIFont systemFontOfSize:18];
    floorSize.text=@"Room Size";
	[scroller addSubview:floorSize];
    
    UILabel *Flooring2 = [[UILabel alloc] initWithFrame:CGRectMake(190,2960,150,35)];
	Flooring2.textColor = [UIColor greenColor];
	Flooring2.backgroundColor = [UIColor clearColor];
	Flooring2.font = [UIFont systemFontOfSize:18];
    Flooring2.text=@"Flooring Type";
	[scroller addSubview:Flooring2];

    UILabel *Fixtures = [[UILabel alloc] initWithFrame:CGRectMake(380,2960,150,35)];
	Fixtures.textColor = [UIColor greenColor];
	Fixtures.backgroundColor = [UIColor clearColor];
	Fixtures.font = [UIFont systemFontOfSize:18];
    Fixtures.text=@"Fixtures";
	[scroller addSubview:Fixtures];

    UILabel *notes2 = [[UILabel alloc] initWithFrame:CGRectMake(515,2960,70,35)];
	notes2.textColor = [UIColor greenColor];
	notes2.backgroundColor = [UIColor clearColor];
    notes2.textAlignment = UITextAlignmentCenter;
	notes2.font = [UIFont systemFontOfSize:18];
    notes2.text=@"Notes";
	[scroller addSubview:notes2];
    
    UILabel *TotalSqft2 = [[UILabel alloc] initWithFrame:CGRectMake(625,2960,90,35)];
	TotalSqft2.textColor = [UIColor greenColor];
	TotalSqft2.backgroundColor = [UIColor clearColor];
	TotalSqft2.font = [UIFont systemFontOfSize:18];
    TotalSqft2.text=@"Total Sq ft.";
	[scroller addSubview:TotalSqft2];
    
    bathroomScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(5,3005,758,330)];
    [bathroomScroller.layer setBorderWidth:1.5];
    [bathroomScroller.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [bathroomScroller.layer setCornerRadius:5.0f];
    [scroller addSubview:bathroomScroller];
    
    UIButton *calSaveBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [calSaveBtn setFrame:CGRectMake(650,3490, 80, 35)];
    [calSaveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [calSaveBtn addTarget:self action:@selector(calSaveBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:calSaveBtn];
    
    UIButton *calculateBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [calculateBtn setFrame:CGRectMake(520,3490, 100, 35)];
    [calculateBtn setTitle:@"Calculate" forState:UIControlStateNormal];
    [calculateBtn addTarget:self action:@selector(calculateBtn) forControlEvents:UIControlEventTouchUpInside];
    [scroller addSubview:calculateBtn];

    calcLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,3525,768,60)];
	calcLabel.textColor = [UIColor greenColor];
    calcLabel.text = @"0";
    calcLabel.numberOfLines = 2;
    calcLabel.textAlignment = UITextAlignmentCenter;
	calcLabel.backgroundColor = [UIColor clearColor];
	calcLabel.font = [UIFont systemFontOfSize:18];
	[scroller addSubview:calcLabel];
    
    y = 0;
    z = 0;
    v = 0;
    
    j = 1;
    k = 1;
    l = 1;

    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    list=[[NSMutableArray alloc] initWithArray:[appDelegate getAllRecords]];

    turnBack = @"Turn back";
    NSMutableDictionary *dict = nil, *proposalDict = nil;

    proposalDict = [appDelegate decodeData:[list objectAtIndex:cellIndex]];
    
    if(cellIndex>-1 && [[proposalDict allKeys] count] > 8){
        email.text=[proposalDict valueForKey:@"Email"];
        identifier.text=[proposalDict valueForKey:@"Identifier"];
        company.text=[proposalDict valueForKey:@"Company"];
        contact.text=[proposalDict valueForKey:@"Contact"];
        phoneNumber.text=[proposalDict valueForKey:@"Phone Number"];
        address.text=[proposalDict valueForKey:@"Address"];
        city.text = [proposalDict valueForKey:@"City"];
        zip.text = [proposalDict valueForKey:@"Zip"];
        salesRep.text=[proposalDict valueForKey:@"Sales Rep"];
        leadSource.text=[proposalDict valueForKey:@"Lead Source"]; 
        frequency.text=[proposalDict valueForKey:@"Frequency"]; 
        [ReasonBtn setTitle:[proposalDict valueForKey:@"ReasonBtn"] forState:UIControlStateNormal];
        adjustedPrice.text=[proposalDict valueForKey:@"AdjustedPrice"];
        price.text=[proposalDict valueForKey:@"Price"];
        [Appointment setTitle:[proposalDict valueForKey:@"Appointment"] forState:UIControlStateNormal];
        manHours.text=[proposalDict valueForKey:@"Man Hours"];
        hourlyRate.text = [proposalDict valueForKey:@"ManHourlyRate"];
        dailyCost.text = [proposalDict valueForKey:@"ManTotalCost"];
        totalSquareFeet.text=[proposalDict valueForKey:@"Total Square Feet"];
        productionRate.text = [proposalDict valueForKey:@"Production Rate"];
        ic.text=[proposalDict valueForKey:@"Ic"];
        [bidDeliveryDate setTitle:[proposalDict valueForKey:@"Bid Delivery Date"] forState:UIControlStateNormal];
        multiprice2.text=[proposalDict valueForKey:@"Multiprice2"];
        multiprice1.text=[proposalDict valueForKey:@"Multiprice1"];
        note.text=[proposalDict valueForKey:@"Note"];
        adjustedDailyCost.text = [proposalDict valueForKey:@"AdjustedDailyCost"];
        curService.text=[proposalDict valueForKey:@"Current Service"];
        cost.text=[proposalDict valueForKey:@"Cost"];
        howManyPeople.text=[proposalDict valueForKey:@"How Many People"];
        howMuchTime.text=[proposalDict valueForKey:@"How Much Time"];
        TbanoofE.text=[proposalDict valueForKey:@"NoOfEmp"]; 
        [visitors setTitle:[proposalDict valueForKey:@"NoOfVisitors"] forState:UIControlStateNormal];
        [rating setTitle:[proposalDict valueForKey:@"Rating"] forState:UIControlStateNormal];
        underContract.text=[proposalDict valueForKey:@"Under Contract"];
        [tillWhen setTitle:[proposalDict valueForKey:@"Till When"] forState:UIControlStateNormal];
        decisionBeMade.text=[proposalDict valueForKey:@"Decision Be Made"];
        byWho.text=[proposalDict valueForKey:@"By Who"];
        perWeek.text=[proposalDict valueForKey:@"Per Week"];
        restrictedHours.text=[proposalDict valueForKey:@"Restricted Hours"];
        priceOfItems.text=[proposalDict valueForKey:@"Price Of Items"];
        hours.text=[proposalDict valueForKey:@"Hours"];
        hourRate.text=[proposalDict valueForKey:@"Hour Rate"];
        notes.text= [proposalDict valueForKey:@"NotesView"];
        [facilitybtn setTitle:[proposalDict valueForKey:@"Facility Type"] forState:UIControlStateNormal];
        totalFixtures.text=[proposalDict valueForKey:@"Total Fixtures"];
        sink.text= [proposalDict valueForKey:@"Sink"];
        micro.text= [proposalDict valueForKey:@"Microwave"];
        refrig.text= [proposalDict valueForKey:@"Refrigerator"];
        vending.text= [proposalDict valueForKey:@"Vending"];
        tables.text= [proposalDict valueForKey:@"Tables"];
        water.text= [proposalDict valueForKey:@"Water Cooler"];
        dish.text= [proposalDict valueForKey:@"Dish Washer"];
        specialItems.text= [proposalDict valueForKey:@"Special Items"];
        costOfSpecial.text= [proposalDict valueForKey:@"Cost of Special"];
        calcLabel.text = [proposalDict valueForKey:@"calcLabel"];
        if([[proposalDict valueForKey:@"Turn Back"] isEqualToString:@""])
            turnBack = @"Turn back";
        else
            turnBack = [proposalDict valueForKey:@"Turn Back"];
        
        if([[proposalDict valueForKey:@"TurnBack Sent"] isEqualToString:@"YES"])
            turnBackSent = [proposalDict valueForKey:@"TurnBack Sent"];
        else
            turnBackSent = @"NO";

        if([[proposalDict valueForKey:@"Submitted"] isEqualToString:@"YES"])
            submitted = @"YES";
        else
            submitted = @"NO";
        
        if([[proposalDict objectForKey:@"BillToDifferent"] isEqualToString:@"Yes"]){
            [billDifferent setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
            [billDifferent setSelected:YES];
        }
        
        if([proposalDict objectForKey:@"Image"])
            imgData = [proposalDict objectForKey:@"Image"];
        
        selectedFaci = [[proposalDict objectForKey:@"Selected Facility"] intValue];
        if([[proposalDict valueForKey:@"Have Keys"] isEqualToString:@"Yes"]){
            UIButton *Radiobutton = (UIButton*) [haveKeys viewWithTag:11];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }else if([[proposalDict valueForKey:@"Have Keys"] isEqualToString:@"No"]){
            UIButton *Radiobutton = (UIButton*) [haveKeys viewWithTag:22];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }
        if([[proposalDict valueForKey:@"Day Porter"] isEqualToString:@"Yes"]){
            UIButton *Radiobutton = (UIButton*) [dayPorter viewWithTag:11];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }else if([[proposalDict valueForKey:@"Day Porter"] isEqualToString:@"No"]){
            UIButton *Radiobutton = (UIButton*) [dayPorter viewWithTag:22];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }
        if([[proposalDict valueForKey:@"Trash Liners"] isEqualToString:@"Client"]){
            UIButton *Radiobutton = (UIButton*) [trashLiners viewWithTag:11];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }else if([[proposalDict valueForKey:@"Trash Liners"] isEqualToString:@"ImageOne"]){
            UIButton *Radiobutton = (UIButton*) [trashLiners viewWithTag:22];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }else if([[proposalDict valueForKey:@"Trash Liners"] isEqualToString:@"We Order"]){
            UIButton *Radiobutton = (UIButton*) [trashLiners viewWithTag:33];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }
        if([[proposalDict valueForKey:@"Bath Consumables"] isEqualToString:@"Client"]){
            UIButton *Radiobutton = (UIButton*) [bthConsumables viewWithTag:11];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }else if([[proposalDict valueForKey:@"Bath Consumables"] isEqualToString:@"ImageOne"]){
            UIButton *Radiobutton = (UIButton*) [bthConsumables viewWithTag:22];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }else if([[proposalDict valueForKey:@"Bath Consumables"] isEqualToString:@"We Order"]){
            UIButton *Radiobutton = (UIButton*) [bthConsumables viewWithTag:33];
            [Radiobutton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        }
        
        NSArray *days = [proposalDict objectForKey:@"Which Days"];
        NSArray *spclAreas = [proposalDict objectForKey:@"Special Areas"];
        for(UIButton *btn in whichDays.subviews)
            if([btn isKindOfClass:[UIButton class]])
                if([days containsObject:btn.titleLabel.text]){
                    [btn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];        
                    [btn setSelected:YES];
                }
        
        for(UIButton *btn in specialAreas.subviews)
            if([btn isKindOfClass:[UIButton class]])
                if([spclAreas containsObject:btn.titleLabel.text]){
                    [btn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];        
                    [btn setSelected:YES];
                }

        for(dict in [proposalDict objectForKey:@"FacilityType"])
            [self addButton1:dict];
        
        for(dict in [proposalDict objectForKey:@"Kitchens"])
            [self addButton2:dict];
        
        for(dict in [proposalDict objectForKey:@"Bathrooms"])
            [self addButton3:dict];
        
        for(NSDictionary *dic in [proposalDict objectForKey:@"fTypes"]){
            if([[dic valueForKey:@"fname"] isEqualToString:@"CPT"]){
                cptTS.text = [dic valueForKey:@"TotalSqft"];
                cptC.text = [dic valueForKey:@"Cost"];
                cptTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"VCT"]){
                vctTS.text = [dic valueForKey:@"TotalSqft"];
                vctC.text = [dic valueForKey:@"Cost"];
                vctTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"CER"]){
                cerTS.text = [dic valueForKey:@"TotalSqft"];
                cerC.text = [dic valueForKey:@"Cost"];
                cerTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"VSG"]){
                vsgTS.text = [dic valueForKey:@"TotalSqft"];
                vsgC.text = [dic valueForKey:@"Cost"];
                vsgTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"CRT"]){
                crtTS.text = [dic valueForKey:@"TotalSqft"];
                crtC.text = [dic valueForKey:@"Cost"];
                crtTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"LIN"]){
                linTS.text = [dic valueForKey:@"TotalSqft"];
                linC.text = [dic valueForKey:@"Cost"];
                linTC.text = [dic valueForKey:@"TotalCost"];
            }
            else if([[dic valueForKey:@"fname"] isEqualToString:@"WOOD"]){
                woodTS.text = [dic valueForKey:@"TotalSqft"];
                woodC.text = [dic valueForKey:@"Cost"];
                woodTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"TRZ"]){
                trzTS.text = [dic valueForKey:@"TotalSqft"];
                trzC.text = [dic valueForKey:@"Cost"];
                trzTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"RUB"]){
                rubTS.text = [dic valueForKey:@"TotalSqft"];
                rubC.text = [dic valueForKey:@"Cost"];
                rubTC.text = [dic valueForKey:@"TotalCost"];
            }else if([[dic valueForKey:@"fname"] isEqualToString:@"EPOXY"]){
                epoxyTS.text = [dic valueForKey:@"TotalSqft"];
                epoxyC.text = [dic valueForKey:@"Cost"];
                epoxyTC.text = [dic valueForKey:@"TotalCost"];
            }
        }
    }
    else{
        [self addButton1:dict];
        [self addButton2:dict];
        [self addButton3:dict];
    }
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissModalViewControllerAnimated:YES];
    
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    imgData = UIImageJPEGRepresentation(img, 1.0);
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	[self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Alert View Delegates
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1 && buttonIndex == 1){
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (id obj in faciltyScroller.subviews)
            if([obj isKindOfClass:[UIView class]]){
                UIView *vw = (UIView *)obj;
                if([vw.subviews count] == 8){
                    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                    UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                    [dict setValue:tf1.text forKey:@"size1"];
                    UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                    [dict setValue:tf2.text forKey:@"size2"];
                    UIButton *rType = (UIButton*) [vw viewWithTag:9997];
                    [dict setValue:rType.titleLabel.text forKey:@"roomType"];
                    UIButton *fType = (UIButton*) [vw viewWithTag:9996];
                    [dict setValue:fType.titleLabel.text forKey:@"floorType"];
                    UITextField *tf3 = (UITextField*) [vw viewWithTag:13000];
                    [dict setValue:tf3.text forKey:@"notes"];
                    UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                    tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                    [dict setValue:tf4.text forKey:@"total1"];
                    [arr addObject:dict];
                }
                [vw removeFromSuperview];
            }
        [arr removeObjectAtIndex:delButton1.tag-1];
        y = 0;
        j = 1;
        
        for(NSMutableDictionary *dict1 in arr)
            [self addButton1:dict1];
    }else if(alertView.tag == 2 && buttonIndex == 1){
        NSMutableArray *arr2 = [[NSMutableArray alloc] init];
        for (id obj in kitchenScroller.subviews)
            if([obj isKindOfClass:[UIView class]]){
                UIView *vw = (UIView *)obj;
                if([vw.subviews count] == 7){
                    NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
                    UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                    [dict2 setValue:tf1.text forKey:@"size3"];
                    UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                    [dict2 setValue:tf2.text forKey:@"size4"];
                    UIButton *btn = (UIButton*) [vw viewWithTag:9996];
                    [dict2 setValue:btn.titleLabel.text forKey:@"floorType"];
                    UITextField *tf = (UITextField*) [vw viewWithTag:13000];
                    [dict2 setValue:tf.text forKey:@"notes"];
                    UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                    tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                    [dict2 setValue:tf4.text forKey:@"total2"];
                    [arr2 addObject:dict2];
                }
                [vw removeFromSuperview];
            }
        [arr2 removeObjectAtIndex:delButton2.tag-1];
        z = 0;
        k = 1;
        
        for(NSMutableDictionary *dict1 in arr2)
            [self addButton2:dict1];
    }else if(alertView.tag == 3 && buttonIndex == 1){
        NSMutableArray *arr3 = [[NSMutableArray alloc] init];
        for (id obj in bathroomScroller.subviews)
            if([obj isKindOfClass:[UIView class]]){
                UIView *vw = (UIView *)obj;
                if([vw.subviews count] == 9){
                    NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
                    UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                    [dict2 setValue:tf1.text forKey:@"size5"];
                    UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                    [dict2 setValue:tf2.text forKey:@"size6"];
                    UIButton *fType = (UIButton*) [vw viewWithTag:9996];
                    [dict2 setValue:fType.titleLabel.text forKey:@"floorType"];
                    UITextField *tf5 = (UITextField*) [vw viewWithTag:15000];
                    [dict2 setValue:tf5.text forKey:@"fixtures"];
                    UITextField *tf = (UITextField*) [vw viewWithTag:13000];
                    [dict2 setValue:tf.text forKey:@"notes1"];
                    UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                    tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                    [dict2 setValue:tf4.text forKey:@"total3"];
                    [arr3 addObject:dict2];
                }
                [vw removeFromSuperview];
            }
        [arr3 removeObjectAtIndex:delButton3.tag-1];
        v = 0;
        l = 1;
        
        for(NSMutableDictionary *dict3 in arr3)
            [self addButton3:dict3];
    }else if(alertView.tag == 999 && buttonIndex == 1 && [dateFormat stringFromDate:[dobPicker date]])
        [selectedBtn setTitle:[dateFormat stringFromDate:[dobPicker date]] forState:UIControlStateNormal];
}

- (void)willPresentAlertView:(UIAlertView *)alertView {
    if(alertView.tag == 999)
        alertView.frame = CGRectMake(242,384,284,240);
}

#pragma mark -
#pragma mark TextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.tag == 15000){
        totalFixtures.text = nil;
        float count = 0;
        for(UIView *vw in bathroomScroller.subviews)
            if([vw.subviews count] == 9){
            UITextField *tf = (UITextField*) [vw viewWithTag:15000];
               if(count == 0) 
                   count = [tf.text floatValue];
               else
                   count = count + [tf.text floatValue];
        }
        totalFixtures.text = [NSString stringWithFormat:@"Total Fixtures : %.2f",count];
    }
    
    if(textField == productionRate){
        if([productionRate.text floatValue] == 0)
            manHours.text = @"";
        else
            manHours.text = [NSString stringWithFormat:@"%.2f",[totalSquareFeet.text floatValue] / [productionRate.text floatValue]];
        dailyCost.text = [NSString stringWithFormat:@"%.2f",[hourlyRate.text floatValue] * [manHours.text floatValue]];
        [self calcMonthlyQuote];
        return;
    }
    if(textField == manHours || textField == hourlyRate){
        if([manHours.text floatValue] == 0 || [totalSquareFeet.text floatValue] == 0)
            productionRate.text = @"";
        else
            productionRate.text = [NSString stringWithFormat:@"%.2f",[totalSquareFeet.text floatValue]/[manHours.text floatValue]];

        dailyCost.text = [NSString stringWithFormat:@"%.2f",[hourlyRate.text floatValue] * [manHours.text floatValue]];
        [self calcMonthlyQuote];
        return;
    }
    
    if(textField.tag == 11000 || textField.tag == 12000){
        UIView *vw = [textField superview];
        UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
        UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
        UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
        tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
        return;
    }else if(textField == cptC || textField == vctC || textField == cerC || textField == vsgC || textField == crtC || textField == linC || textField == woodC || textField == trzC || textField ==rubC || textField == epoxyC || textField == hourlyRate || textField == manHours || textField == hours || textField == hourRate || textField == perWeek || textField == priceOfItems || textField == costOfSpecial){
        if(textField == cptC){
            if([cptTS.text floatValue]>0 && [cptC.text floatValue]>0)
                cptTC.text = [NSString stringWithFormat:@"%.2f",[cptC.text floatValue] * [cptTS.text floatValue]];
            else
                cptTC.text = @"";
            return;
        }
        if(textField == vctC){
            if([vctTS.text floatValue]>0 && [vctC.text floatValue]>0)
                vctTC.text = [NSString stringWithFormat:@"%.2f",[vctC.text floatValue] * [vctTS.text floatValue]];
            else
                vctTC.text = @"";
            return;
        }
        if(textField == cerC){
            if([cerTS.text floatValue]>0 && [cerC.text floatValue]>0)
                cerTC.text = [NSString stringWithFormat:@"%.2f",[cerC.text floatValue] * [cerTS.text floatValue]];
            else
                cerTC.text = @"";
            return;
        }
        if(textField == vsgC){
            if([vsgTS.text floatValue]>0 && [vsgC.text floatValue]>0)
                vsgTC.text = [NSString stringWithFormat:@"%.2f",[vsgC.text floatValue] * [vsgTS.text floatValue]];
            else
                vsgTC.text = @"";
            return;
        }
        if(textField == crtC){
            if([crtTS.text floatValue]>0 && [crtC.text floatValue])
                crtTC.text = [NSString stringWithFormat:@"%.2f",[crtC.text floatValue] * [crtTS.text floatValue]];
            else
                crtTC.text = @"";
            return;
        }
        if(textField == linC){
            if([linTS.text floatValue]>0 && [linC.text floatValue]>0)
                linTC.text = [NSString stringWithFormat:@"%.2f",[linC.text floatValue] * [linTS.text floatValue]];
            else
                linTC.text = @"";
            return;
        }
        if(textField == woodC){
            if([woodTS.text floatValue]>0 && [woodC.text floatValue]>0)
                woodTC.text = [NSString stringWithFormat:@"%.2f",[woodC.text floatValue] * [woodTS.text floatValue]];
            else
                woodTC.text = @"";
            return;
        }
        if(textField == trzC){
            if([trzTS.text floatValue]>0 && [trzC.text floatValue]>0)
                trzTC.text = [NSString stringWithFormat:@"%.2f",[trzC.text floatValue] * [trzTS.text floatValue]];
            else
                trzTC.text = @"";
            return;
        }
        if(textField ==  rubC){
            if([ rubTS.text floatValue]>0 && [rubC.text floatValue]>0)
                rubTC.text = [NSString stringWithFormat:@"%.2f",[rubC.text floatValue] * [rubTS.text floatValue]];
            else
                rubTC.text = @"";
            return;
        }
        if(textField ==  epoxyC){
            if([ epoxyTS.text floatValue]>0 && [epoxyC.text floatValue]>0)
                epoxyTC.text = [NSString stringWithFormat:@"%.2f",[epoxyC.text floatValue] * [epoxyTS.text floatValue]];
            else
                epoxyTC.text = @"";
            return;
        }
        else if(textField == hourlyRate || textField == hours || textField == hourRate || textField == perWeek || textField == priceOfItems || textField == costOfSpecial){
            if(textField == perWeek)
                frequency.text = textField.text;
            [self calcMonthlyQuote];
        }
    }
}

#pragma mark - UIButton methods

- (IBAction)calBtnPressed:(id)sender{
    [self calculateBtn];
}

-(IBAction)camera:(id)sender{
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
		UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
		imagePickerController.delegate = self;
		imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentModalViewController:imagePickerController animated:YES];
        imagePickerController = nil;
    }
}

-(void)addButton1:(NSMutableDictionary *)dict{
    UIView *thirdView=[[UIView alloc] initWithFrame:CGRectMake(0,y,758,40)];
    thirdView.tag = j;
    
    if(j%2)
        thirdView.backgroundColor = teal;
    
    y = y+41;
    [faciltyScroller setContentSize:CGSizeMake(750, y)];
    
    UITextField *txtField = [[UITextField alloc] initWithFrame:CGRectMake(9,6.5,65,28)];
    txtField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField.backgroundColor = [UIColor whiteColor];
    txtField.font = [UIFont systemFontOfSize:16];
    txtField.keyboardType = UIKeyboardTypeNumberPad;
    txtField.textAlignment = UITextAlignmentCenter;
    txtField.tag =11000;
    [[txtField layer] setCornerRadius:5.0];
    txtField.borderStyle = UITextBorderStyleRoundedRect;
    txtField.delegate = self;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField.text = [dict valueForKey:@"size1"];
    else if([dict isKindOfClass:[UIButton class]])
        [txtField becomeFirstResponder];
    else
        txtField.text = @"";
    [thirdView addSubview:txtField];
    
    UILabel *txtLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(77,6.5,20,28)];
	txtLabel1.textColor = [UIColor greenColor];
	txtLabel1.backgroundColor = [UIColor clearColor];
	txtLabel1.font = [UIFont systemFontOfSize:17];
    txtLabel1.text=@"X";
	[thirdView addSubview:txtLabel1];
    
    UITextField *txtField2 = [[UITextField alloc] initWithFrame:CGRectMake(91,6.5,65,28)];
    txtField2.backgroundColor = [UIColor whiteColor];
    txtField2.font = [UIFont systemFontOfSize:16];
    txtField2.keyboardType = UIKeyboardTypeNumberPad;
    txtField2.textAlignment = UITextAlignmentCenter;
    txtField2.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField2.tag =12000;
    [[txtField2 layer] setCornerRadius:5.0];
    txtField2.borderStyle = UITextBorderStyleRoundedRect;
    txtField2.delegate = self;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField2.text = [dict valueForKey:@"size2"];
    else
        txtField2.text = @"";
    [thirdView addSubview:txtField2];
    
    roomTypebtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [roomTypebtn setFrame:CGRectMake(170,6.5, 145, 28)];
    roomTypebtn.tag=9997;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        [roomTypebtn setTitle:[dict valueForKey:@"roomType"] forState:UIControlStateNormal];
    else
        [roomTypebtn setTitle:@"Select" forState:UIControlStateNormal];
    [roomTypebtn addTarget:self action:@selector(TableAlertButton:) forControlEvents:UIControlEventTouchUpInside];
    [thirdView addSubview:roomTypebtn];
    
    floorTypebtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [floorTypebtn setFrame:CGRectMake(345,6.5, 100, 28)];
    floorTypebtn.tag=9996;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        [floorTypebtn setTitle:[dict valueForKey:@"floorType"] forState:UIControlStateNormal];
    else
        [floorTypebtn setTitle:@"Select" forState:UIControlStateNormal];
    [floorTypebtn addTarget:self action:@selector(TableAlertButton:) forControlEvents:UIControlEventTouchUpInside];
    [thirdView addSubview:floorTypebtn];
    
    UITextField *txtField4 = [[UITextField alloc] initWithFrame:CGRectMake(475,6.5,120,28)];
    txtField4.backgroundColor = [UIColor whiteColor];
    txtField4.font = [UIFont systemFontOfSize:16];
    txtField4.textAlignment = UITextAlignmentCenter;
    txtField4.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField4.tag =13000;
    txtField4.delegate = self;
    [[txtField4 layer] setCornerRadius:5.0];
    txtField4.borderStyle = UITextBorderStyleRoundedRect;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField4.text = [dict valueForKey:@"notes"];
    else
        txtField4.text = @"";
    [thirdView addSubview:txtField4];
    
    UILabel *total = [[UILabel alloc] initWithFrame:CGRectMake(615,6.5,90,28)];
	total.textColor = [UIColor greenColor];
    total.tag = 14000;
	total.backgroundColor = [UIColor clearColor];
    total.textAlignment = UITextAlignmentCenter;
	total.font = [UIFont systemFontOfSize:18];
    if([dict isKindOfClass:[NSMutableDictionary class]])
        total.text = [dict valueForKey:@"total1"];
    else
        total.text=@"0";
	[thirdView addSubview:total];
    
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [deleteBtn setFrame:CGRectMake(715,6.5, 25, 28)];
    deleteBtn.tag=j;
    [deleteBtn setTitle:@"X" forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteButton1:) forControlEvents:UIControlEventTouchUpInside];
    [thirdView addSubview:deleteBtn];
    [faciltyScroller addSubview:thirdView];
    if(j > 10)
        [faciltyScroller setContentOffset:CGPointMake(0, y-410) animated:YES];
    j++;
}

-(void)addButton2:(NSMutableDictionary *)dict{
    UIView *thirdView=[[UIView alloc] initWithFrame:CGRectMake(0,z,758,40)];
    thirdView.tag = k;
    
    if(k%2)
        thirdView.backgroundColor = teal;

    z = z+41;
    [kitchenScroller setContentSize:CGSizeMake(750, z)];
    
    UITextField *txtField1 = [[UITextField alloc] initWithFrame:CGRectMake(9,6.5,65,28)];
    txtField1.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField1.backgroundColor = [UIColor whiteColor];
    txtField1.font = [UIFont systemFontOfSize:16];
    txtField1.keyboardType = UIKeyboardTypeNumberPad;
    txtField1.textAlignment = UITextAlignmentCenter;
    txtField1.tag =11000;
    [[txtField1 layer] setCornerRadius:5.0];
    txtField1.borderStyle = UITextBorderStyleRoundedRect;
    txtField1.delegate = self;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField1.text = [dict valueForKey:@"size3"];
    else if([dict isKindOfClass:[UIButton class]])
        [txtField1 becomeFirstResponder];
    else
        txtField1.text = @"";
    [thirdView addSubview:txtField1];
    
    UILabel *txtLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(77,6.5,20,28)];
	txtLabel1.textColor = [UIColor greenColor];
	txtLabel1.backgroundColor = [UIColor clearColor];
	txtLabel1.font = [UIFont systemFontOfSize:17];
    txtLabel1.text=@"X";
	[thirdView addSubview:txtLabel1];
    
    UITextField *txtField2 = [[UITextField alloc] initWithFrame:CGRectMake(91,6.5,65,28)];
    txtField2.backgroundColor = [UIColor whiteColor];
    txtField2.font = [UIFont systemFontOfSize:16];
    txtField2.keyboardType = UIKeyboardTypeNumberPad;
    txtField2.textAlignment = UITextAlignmentCenter;
    txtField2.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField2.tag =12000;
    [[txtField2 layer] setCornerRadius:5.0];
    txtField2.borderStyle = UITextBorderStyleRoundedRect;
    txtField2.delegate = self;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField2.text = [dict valueForKey:@"size4"];
    else
        txtField2.text = @"";
    [thirdView addSubview:txtField2];
    
    kitchenbtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [kitchenbtn setFrame:CGRectMake(230,6.5, 120, 28)];
    kitchenbtn.tag=9996;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        [kitchenbtn setTitle:[dict valueForKey:@"floorType"] forState:UIControlStateNormal];
    else
        [kitchenbtn setTitle:@"Select" forState:UIControlStateNormal];
    [kitchenbtn addTarget:self action:@selector(TableAlertButton:) forControlEvents:UIControlEventTouchUpInside];
    [thirdView addSubview:kitchenbtn];
    
    UITextField *txtField3 = [[UITextField alloc] initWithFrame:CGRectMake(400,6.5,200,28)];
    txtField3.backgroundColor = [UIColor whiteColor];
    txtField3.textAlignment = UITextAlignmentCenter;
    txtField3.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField3.tag =13000;
    txtField3.font = [UIFont systemFontOfSize:16];
    txtField3.delegate = self;
    [[txtField3 layer] setCornerRadius:5.0];
    txtField3.borderStyle = UITextBorderStyleRoundedRect;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField3.text = [dict valueForKey:@"notes"];
    else
        txtField3.text = @"";
    [thirdView addSubview:txtField3];

    UILabel *total = [[UILabel alloc] initWithFrame:CGRectMake(615,6.5,90,28)];
	total.textColor = [UIColor greenColor];
    total.tag = 14000;
	total.backgroundColor = [UIColor clearColor];
    total.textAlignment = UITextAlignmentCenter;
	total.font = [UIFont systemFontOfSize:18];
    if([dict isKindOfClass:[NSMutableDictionary class]])
        total.text = [dict valueForKey:@"total2"];
    else
        total.text=@"0";
	[thirdView addSubview:total];
    
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [deleteBtn setFrame:CGRectMake(715,6.5, 25, 28)];
    deleteBtn.tag=k;
    [deleteBtn setTitle:@"X" forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteButton2:) forControlEvents:UIControlEventTouchUpInside];
    [thirdView addSubview:deleteBtn];
    [kitchenScroller addSubview:thirdView];
    if(k > 4)
        [kitchenScroller setContentOffset:CGPointMake(0, z-164) animated:YES];
    k++;
}

-(void)addButton3:(NSMutableDictionary *)dict{
    UIView *thirdView=[[UIView alloc] initWithFrame:CGRectMake(0,v,758,40)];
    thirdView.tag = l;
    
    if(l%2)
        thirdView.backgroundColor = teal;

    v = v+41;
    [bathroomScroller setContentSize:CGSizeMake(750, v)];

    UITextField *txtField1 = [[UITextField alloc] initWithFrame:CGRectMake(10,6.5,65,28)];
    txtField1.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField1.backgroundColor = [UIColor whiteColor];
    txtField1.font = [UIFont systemFontOfSize:16];
    txtField1.keyboardType = UIKeyboardTypeNumberPad;
    txtField1.textAlignment = UITextAlignmentCenter;
    txtField1.tag =11000;
    [[txtField1 layer] setCornerRadius:5.0];
    txtField1.borderStyle = UITextBorderStyleRoundedRect;
    txtField1.delegate = self;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField1.text = [dict valueForKey:@"size5"];
    else if([dict isKindOfClass:[UIButton class]])
        [txtField1 becomeFirstResponder];
    else
        txtField1.text = @"";
    [thirdView addSubview:txtField1];
    
    UILabel *txtLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(77,6.5,20,28)];
	txtLabel1.textColor = [UIColor greenColor];
	txtLabel1.backgroundColor = [UIColor clearColor];
	txtLabel1.font = [UIFont systemFontOfSize:17];
    txtLabel1.text=@"X";
	[thirdView addSubview:txtLabel1];
    
    UITextField *txtField2 = [[UITextField alloc] initWithFrame:CGRectMake(91,6.5,65,28)];
    txtField2.backgroundColor = [UIColor whiteColor];
    txtField2.font = [UIFont systemFontOfSize:16];
    txtField2.keyboardType = UIKeyboardTypeNumberPad;
    txtField2.textAlignment = UITextAlignmentCenter;
    txtField2.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField2.tag =12000;
    [[txtField2 layer] setCornerRadius:5.0];
    txtField2.borderStyle = UITextBorderStyleRoundedRect;
    txtField2.delegate = self;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField2.text = [dict valueForKey:@"size6"];
    else
        txtField2.text = @"";
    [thirdView addSubview:txtField2];
    
    floorTypebtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [floorTypebtn setFrame:CGRectMake(185,6.5, 100, 28)];
    floorTypebtn.tag=9996;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        [floorTypebtn setTitle:[dict valueForKey:@"floorType"] forState:UIControlStateNormal];
    else
        [floorTypebtn setTitle:@"Select" forState:UIControlStateNormal];
    [floorTypebtn addTarget:self action:@selector(TableAlertButton:) forControlEvents:UIControlEventTouchUpInside];
    [thirdView addSubview:floorTypebtn];

    UILabel *fixture = [[UILabel alloc] initWithFrame:CGRectMake(330,6.5,90,28)];
	fixture.textColor = [UIColor greenColor];
	fixture.backgroundColor = [UIColor clearColor];
    fixture.textAlignment = UITextAlignmentCenter;
	fixture.font = [UIFont systemFontOfSize:18];
    fixture.text = @"How many";
	[thirdView addSubview:fixture];

    UITextField *fixtureTF = [[UITextField alloc] initWithFrame:CGRectMake(425,6.5,40,28)];
    fixtureTF.backgroundColor = [UIColor whiteColor];
    fixtureTF.tag = 15000;
    fixtureTF.delegate = self;
    fixtureTF.font = [UIFont systemFontOfSize:16];
    fixtureTF.textAlignment = UITextAlignmentCenter;
    fixtureTF.keyboardType = UIKeyboardTypeNumberPad;
    [[fixtureTF layer] setCornerRadius:5.0];
    fixtureTF.borderStyle = UITextBorderStyleRoundedRect;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        fixtureTF.text = [dict valueForKey:@"fixtures"];
    else
        fixtureTF.text = @"";
    [thirdView addSubview:fixtureTF];

    UITextField *txtField4 = [[UITextField alloc] initWithFrame:CGRectMake(490,6.5,100,28)];
    txtField4.backgroundColor = [UIColor whiteColor];
    txtField4.textAlignment = UITextAlignmentCenter;
    txtField4.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    txtField4.tag =13000;
    txtField4.delegate = self;
    txtField4.font = [UIFont systemFontOfSize:16];
    [[txtField4 layer] setCornerRadius:5.0];
    txtField4.borderStyle = UITextBorderStyleRoundedRect;
    if([dict isKindOfClass:[NSMutableDictionary class]])
        txtField4.text = [dict valueForKey:@"notes1"];
    else
        txtField4.text = @"";
    [thirdView addSubview:txtField4];
    
    UILabel *total = [[UILabel alloc] initWithFrame:CGRectMake(605,6.5,90,28)];
	total.textColor = [UIColor greenColor];
    total.tag = 14000;
	total.backgroundColor = [UIColor clearColor];
    total.textAlignment = UITextAlignmentCenter;
	total.font = [UIFont systemFontOfSize:18];
    if([dict isKindOfClass:[NSMutableDictionary class]])
        total.text = [dict valueForKey:@"total3"];
    else
        total.text=@"0";
	[thirdView addSubview:total];
    
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [deleteBtn setFrame:CGRectMake(715,6.5, 25, 28)];
    deleteBtn.tag=l;
    [deleteBtn setTitle:@"X" forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteButton3:) forControlEvents:UIControlEventTouchUpInside];
    [thirdView addSubview:deleteBtn];
    [bathroomScroller addSubview:thirdView];
    if(l > 8)
        [bathroomScroller setContentOffset:CGPointMake(0, v-330) animated:YES];
    l++;
}

-(void)dupButton:(id)sender{
    UIButton *btn = (UIButton *)sender;
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    switch (btn.tag) {
        case 1:
            for (id obj in faciltyScroller.subviews)
                if([obj isKindOfClass:[UIView class]]){
                    UIView *vw = (UIView *)obj;
                    if([vw.subviews count] == 8){
                        UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                        [dict setValue:tf1.text forKey:@"size1"];
                        UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                        [dict setValue:tf2.text forKey:@"size2"];
                        UIButton *rType = (UIButton*) [vw viewWithTag:9997];
                        [dict setValue:rType.titleLabel.text forKey:@"roomType"];
                        UIButton *fType = (UIButton*) [vw viewWithTag:9996];
                        [dict setValue:fType.titleLabel.text forKey:@"floorType"];
                        UITextField *tf3 = (UITextField*) [vw viewWithTag:13000];
                        [dict setValue:tf3.text forKey:@"notes"];
                        UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                        tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                        [dict setValue:tf4.text forKey:@"total1"];
                    }
                }
            [self addButton1:dict];
            break;
        case 2:
            for (id obj in kitchenScroller.subviews)
                if([obj isKindOfClass:[UIView class]]){
                    UIView *vw = (UIView *)obj;
                    if([vw.subviews count] == 7){
                        UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                        [dict setValue:tf1.text forKey:@"size3"];
                        UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                        [dict setValue:tf2.text forKey:@"size4"];
                        UIButton *btn = (UIButton*) [vw viewWithTag:9996];
                        [dict setValue:btn.titleLabel.text forKey:@"floorType"];
                        UITextField *tf = (UITextField*) [vw viewWithTag:13000];
                        [dict setValue:tf.text forKey:@"notes"];
                        UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                        tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                        [dict setValue:tf4.text forKey:@"total2"];
                    }
                }
            [self addButton2:dict];
            break;
            
        default:
            for (id obj in bathroomScroller.subviews)
                if([obj isKindOfClass:[UIView class]]){
                    UIView *vw = (UIView *)obj;
                    if([vw.subviews count] == 9){
                        UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                        [dict setValue:tf1.text forKey:@"size5"];
                        UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                        [dict setValue:tf2.text forKey:@"size6"];
                        UIButton *fType = (UIButton*) [vw viewWithTag:9996];
                        [dict setValue:fType.titleLabel.text forKey:@"floorType"];
                        UITextField *tf5 = (UITextField*) [vw viewWithTag:15000];
                        [dict setValue:tf5.text forKey:@"fixtures"];
                        UITextField *tf = (UITextField*) [vw viewWithTag:13000];
                        [dict setValue:tf.text forKey:@"notes1"];
                        UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                        tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                        [dict setValue:tf4.text forKey:@"total3"];
                    }
                }
            [self addButton3:dict];
            break;
    }
}

-(void)deleteButton1:(id)sender {
    delButton1 = (UIButton*)sender;
    UIAlertView *alrt1 = [[UIAlertView alloc] initWithTitle:@"Image One" message:@"Do you want to delete this row?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alrt1.tag = 1;
	[alrt1 show];
}

-(void)deleteButton2:(id)sender {
    delButton2 = (UIButton*)sender;
    UIAlertView *alrt2 = [[UIAlertView alloc] initWithTitle:@"Image One" message:@"Do you want to delete this row?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alrt2.tag = 2;
	[alrt2 show];
}

-(void)deleteButton3:(id)sender{
    delButton3 = (UIButton*)sender;
    UIAlertView *alrt3 = [[UIAlertView alloc] initWithTitle:@"Image One" message:@"Do you want to delete this row?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alrt3.tag = 3;
	[alrt3 show];
}

-(void)calculateBtn{
    cptTS.text = nil;      // cptTC.text = nil;       cptC.text = nil;
    vctTS.text = nil;      // vctTC.text = nil;       vctC.text = nil;
    cerTS.text = nil;      // cerTC.text = nil;       cerC.text = nil;
    vsgTS.text = nil;     //  vsgTC.text = nil;      vsgC.text = nil;
    crtTS.text = nil;       // crtTC.text = nil;       crtC.text = nil; 
    linTS.text = nil;       //  linTC.text = nil;       linC.text = nil;
    woodTS.text = nil;   //  woodTC.text = nil;   woodC.text = nil;
    trzTS.text = nil;      //   trzTC.text = nil;      trzC.text = nil;
    rubTS.text = nil;     //  rubTC.text = nil;      rubC.text = nil;
    epoxyTS.text = nil;
    
    totalSquareFeet.text = nil;
    
    for (id obj in faciltyScroller.subviews)
        if([obj isKindOfClass:[UIView class]]){
            UIView *vw = (UIView *)obj;
            if([vw.subviews count] == 8){
                UIButton *fType = (UIButton*) [vw viewWithTag:9996];
                if([fType.titleLabel.text isEqualToString:@"Select"])
                    continue;
                [self totSqfeet:vw];
            }
        }
    for (id obj in kitchenScroller.subviews)
        if([obj isKindOfClass:[UIView class]]){
            UIView *vw = (UIView *)obj;
            if([vw.subviews count] == 7){
                UIButton *fType = (UIButton*) [vw viewWithTag:9996];                
                if([fType.titleLabel.text isEqualToString:@"Select"])
                    continue;
                [self totSqfeet:vw];
            }
        }
    for (id obj in bathroomScroller.subviews)
        if([obj isKindOfClass:[UIView class]]){
            UIView *vw = (UIView *)obj;
            if([vw.subviews count] == 9){
                UIButton *fType = (UIButton*) [vw viewWithTag:9996];                
                if([fType.titleLabel.text isEqualToString:@"Select"])
                    continue;
                [self totSqfeet:vw];
            }
        }
    
    totalFixtures.text = nil;
    float count = 0;
    for(UIView *vw in bathroomScroller.subviews)
        if([vw.subviews count] == 9){
            UITextField *tf = (UITextField*) [vw viewWithTag:15000];
            if(count == 0) 
                count = [tf.text floatValue];
            else
                count = count + [tf.text floatValue];
        }
    totalFixtures.text = [NSString stringWithFormat:@"Total Fixtures : %.2f",count];
    
    if([productionRate.text floatValue] == 0)
        manHours.text = @"";
    else
        manHours.text = [NSString stringWithFormat:@"%.2f",[totalSquareFeet.text floatValue] / [productionRate.text floatValue]];
    dailyCost.text = [NSString stringWithFormat:@"%.2f",[hourlyRate.text floatValue] * [manHours.text floatValue]];

    [self calcMonthlyQuote];
    
    if([cptC.text floatValue]>0 && [cptTS.text floatValue]>0)
        cptTC.text = [NSString stringWithFormat:@"%.2f",[cptC.text floatValue] * [cptTS.text floatValue]];
    else
        cptTC.text = @"";
    if([vctC.text floatValue]>0 && [vctTS.text floatValue]>0)
        vctTC.text = [NSString stringWithFormat:@"%.2f",[vctC.text floatValue] * [vctTS.text floatValue]];
    else
        vctTC.text = @"";
    if([cerC.text floatValue]>0 && [cerTS.text floatValue]>0)
        cerTC.text = [NSString stringWithFormat:@"%.2f",[cerC.text floatValue] * [cerTS.text floatValue]];
    else
        cerTC.text = @"";
    if([vsgC.text floatValue]>0 && [vsgTS.text floatValue]>0)
        vsgTC.text = [NSString stringWithFormat:@"%.2f",[vsgC.text floatValue] * [vsgTS.text floatValue]];
    else
        vsgTC.text = @"";
    if([crtC.text floatValue]>0 && [crtTS.text floatValue]>0)
        crtTC.text = [NSString stringWithFormat:@"%.2f",[crtC.text floatValue] * [crtTS.text floatValue]];
    else
        crtTC.text = @"";
    if([linC.text floatValue]>0 && [linTS.text floatValue]>0)
        linTC.text = [NSString stringWithFormat:@"%.2f",[linC.text floatValue] * [linTS.text floatValue]];
    else
        linTC.text = @"";
    if([woodC.text floatValue]>0 && [woodTS.text floatValue]>0)
        woodTC.text = [NSString stringWithFormat:@"%.2f",[woodC.text floatValue] * [woodTS.text floatValue]];
    else
        woodTC.text = @"";
    if([trzC.text floatValue]>0 && [trzTS.text floatValue]>0)
        trzTC.text = [NSString stringWithFormat:@"%.2f",[trzC.text floatValue] * [trzTS.text floatValue]];
    else
        trzTC.text = @"";
    if([rubC.text floatValue]>0 && [rubTS.text floatValue]>0)
        rubTC.text = [NSString stringWithFormat:@"%.2f",[rubC.text floatValue] * [rubTS.text floatValue]];
    else
        rubTC.text = @"";
    if([epoxyC.text floatValue]>0 && [epoxyTS.text floatValue]>0)
        epoxyTC.text = [NSString stringWithFormat:@"%.2f",[epoxyC.text floatValue] * [epoxyTS.text floatValue]];
    else
        epoxyTC.text = @"";
    
    [self performSelector:@selector(saveData) withObject:self afterDelay:0.2];
}

-(void)calcMonthlyQuote{
    UIButton *btn = (UIButton*) [dayPorter viewWithTag:11];
    if([btn currentImage] == [UIImage imageNamed:@"delete.png"]){
        price.text = [NSString stringWithFormat:@"%.2f",([dailyCost.text floatValue]*[frequency.text floatValue]*4.33) + 
                      ([[[totalFixtures.text componentsSeparatedByString:@": "] lastObject]floatValue]*0.0333*[hourlyRate.text floatValue]*[frequency.text floatValue]*4.33) +
                      ([hours.text floatValue]*[hourRate.text floatValue]*[frequency.text floatValue]*4.33) + 
                      [priceOfItems.text floatValue] + [costOfSpecial.text floatValue]];
        calcLabel.text= [NSString stringWithFormat:@"(%@ * %@ * 4.33) + (%@ * 0.0333 * %@ * %@ * 4.33) + (%@ * %@ * %@ * 4.33) + %@ + %@ = %@",dailyCost.text,frequency.text,[[totalFixtures.text componentsSeparatedByString:@": "] lastObject],hourlyRate.text,frequency.text,hours.text,hourRate.text,frequency.text,priceOfItems.text,costOfSpecial.text,price.text];
        UIButton *btn1 = (UIButton*)[haveKeys viewWithTag:22];
        if([btn1 currentImage] == [UIImage imageNamed:@"delete.png"]){
            price.text = [NSString stringWithFormat:@"%.2f",(([dailyCost.text floatValue]+([dailyCost.text floatValue]*0.2))*[frequency.text floatValue]*4.33)+([[[totalFixtures.text componentsSeparatedByString:@": "] lastObject]floatValue]*0.0333*[hourlyRate.text floatValue]*[frequency.text floatValue]*4.33) +([hours.text floatValue]*[hourRate.text floatValue]*[frequency.text floatValue]*4.33)+[priceOfItems.text floatValue] + [costOfSpecial.text floatValue]];
            calcLabel.text= [NSString stringWithFormat:@"((%@ + (%@ * 0.2)) * %@ * 4.33) + (%@ * 0.0333 * %@ * %@ * 4.33) + (%@ * %@ * %@ * 4.33) + %@ + %@ = %@",dailyCost.text,dailyCost.text,frequency.text,[[totalFixtures.text componentsSeparatedByString:@": "] lastObject],hourlyRate.text,frequency.text,hours.text,hourRate.text,frequency.text,priceOfItems.text,costOfSpecial.text,price.text];
        }
        if([price.text floatValue] == 0 || [frequency.text floatValue] == 0)
            adjustedDailyCost.text = @"";
        else
            adjustedDailyCost.text = [NSString stringWithFormat:@"%.2f",[price.text floatValue] / (4.33*[frequency.text floatValue])];
    }else{
        price.text = [NSString stringWithFormat:@"%.2f",([dailyCost.text floatValue]*[frequency.text floatValue]*4.33) + 
                      ([[[totalFixtures.text componentsSeparatedByString:@": "] lastObject]floatValue]*0.0333*[hourlyRate.text floatValue]*[frequency.text floatValue]*4.33) +
                      [priceOfItems.text floatValue] + [costOfSpecial.text floatValue]];
        calcLabel.text= [NSString stringWithFormat:@"(%@ * %@ * 4.33) + (%@ * 0.0333 * %@ * %@ * 4.33) + %@ + %@ = %@",dailyCost.text,frequency.text,[[totalFixtures.text componentsSeparatedByString:@": "] lastObject],hourlyRate.text,frequency.text,priceOfItems.text,costOfSpecial.text,price.text];
        UIButton *btn1 = (UIButton*)[haveKeys viewWithTag:22];
        if([btn1 currentImage] == [UIImage imageNamed:@"delete.png"]){
            price.text = [NSString stringWithFormat:@"%.2f",(([dailyCost.text floatValue]+([dailyCost.text floatValue]*0.2))*[frequency.text floatValue]*4.33)+([[[totalFixtures.text componentsSeparatedByString:@": "] lastObject]floatValue]*0.0333*[hourlyRate.text floatValue]*[frequency.text floatValue]*4.33)+[priceOfItems.text floatValue]+[costOfSpecial.text floatValue]];
            calcLabel.text = [NSString stringWithFormat:@"((%@ + (%@ * 0.2)) * %@ * 4.33) + (%@ * 0.0333 * %@ * %@ * 4.33) + %@ + %@ = %@",dailyCost.text,dailyCost.text,frequency.text,[[totalFixtures.text componentsSeparatedByString:@": "] lastObject],hourlyRate.text,frequency.text,priceOfItems.text,costOfSpecial.text,price.text];
        }
        if([price.text floatValue] == 0 || [frequency.text floatValue] == 0)
            adjustedDailyCost.text = @"";
        else
            adjustedDailyCost.text = [NSString stringWithFormat:@"%.2f",[price.text floatValue] / (4.33*[frequency.text floatValue])];
    }
} 

-(void)totSqfeet:(UIView*)vw{
    UIButton *fType = (UIButton*) [vw viewWithTag:9996];
    UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
    
    if([totalSquareFeet.text intValue] == 0)
        totalSquareFeet.text = tf4.text;
    else
        totalSquareFeet.text = [NSString stringWithFormat:@"%.2f",[totalSquareFeet.text floatValue] + [tf4.text floatValue]];
    
    if([fType.titleLabel.text isEqualToString:@"CPT"]){
        if([cptTS.text floatValue] == 0)
            cptTS.text = tf4.text;
        else
            cptTS.text = [NSString stringWithFormat:@"%.2f",[cptTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"VCT"]){
        if([vctTS.text floatValue] == 0)
            vctTS.text = tf4.text;
        else
            vctTS.text = [NSString stringWithFormat:@"%.2f",[vctTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"CER"]){
        if([cerTS.text floatValue] == 0)
            cerTS.text = tf4.text;
        else
            cerTS.text = [NSString stringWithFormat:@"%.2f",[cerTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"VSG"]){
        if([vsgTS.text floatValue] == 0)
            vsgTS.text = tf4.text;
        else
            vsgTS.text = [NSString stringWithFormat:@"%.2f",[vsgTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"CRT"]){
        if([crtTS.text floatValue] == 0)
            crtTS.text = tf4.text;
        else
            crtTS.text = [NSString stringWithFormat:@"%.2f",[crtTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"LIN"]){
        if([linTS.text floatValue] == 0)
            linTS.text = tf4.text;
        else
            linTS.text = [NSString stringWithFormat:@"%.2f",[linTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"WOOD"]){
        if([woodTS.text floatValue] == 0)
            woodTS.text = tf4.text;
        else
            woodTS.text = [NSString stringWithFormat:@"%.2f",[woodTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"TRZ"]){
        if([trzTS.text floatValue] == 0)
            trzTS.text = tf4.text;
        else
            trzTS.text = [NSString stringWithFormat:@"%.2f",[trzTS.text floatValue] + [tf4.text floatValue]];
    }
    else if([fType.titleLabel.text isEqualToString:@"RUB"]){
        if([rubTS.text floatValue] == 0)
            rubTS.text = tf4.text;
        else
            rubTS.text = [NSString stringWithFormat:@"%.2f",[rubTS.text floatValue] + [tf4.text floatValue]];
    }else{
        if([epoxyTS.text floatValue] == 0)
            epoxyTS.text = tf4.text;
        else
            epoxyTS.text = [NSString stringWithFormat:@"%.2f",[epoxyTS.text floatValue] + [tf4.text floatValue]];
    }
}

- (IBAction)submitBtnPressed:(id)sender {
    [self calculateBtn];
}

-(void)saveData{
    NSMutableDictionary *proposalDict=[NSMutableDictionary new];
    [proposalDict setValue:email.text forKey:@"Email"];
    [proposalDict setValue:identifier.text forKey:@"Identifier"];
    [proposalDict setValue:company.text forKey:@"Company"];
    [proposalDict setValue:contact.text forKey:@"Contact"];
    [proposalDict setValue:phoneNumber.text forKey:@"Phone Number"];
    [proposalDict setValue:address.text forKey:@"Address"];
    [proposalDict setValue:city.text forKey:@"City"];
    [proposalDict setValue:zip.text forKey:@"Zip"];
    [proposalDict setValue:salesRep.text forKey:@"Sales Rep"];
    [proposalDict setValue:leadSource.text forKey:@"Lead Source"];
    [proposalDict setValue:frequency.text forKey:@"Frequency"];
    [proposalDict setValue:ReasonBtn.titleLabel.text forKey:@"ReasonBtn"];
    [proposalDict setValue:adjustedPrice.text forKey:@"AdjustedPrice"];
    [proposalDict setValue:price.text forKey:@"Price"];
    [proposalDict setValue:Appointment.titleLabel.text forKey:@"Appointment"];
    [proposalDict setValue:manHours.text forKey:@"Man Hours"];
    [proposalDict setValue:hourlyRate.text forKey:@"ManHourlyRate"];
    [proposalDict setValue:dailyCost.text forKey:@"ManTotalCost"];
    [proposalDict setValue:totalSquareFeet.text forKey:@"Total Square Feet"];
    [proposalDict setValue:productionRate.text forKey:@"Production Rate"];
    [proposalDict setValue:ic.text forKey:@"Ic"];
    [proposalDict setValue:bidDeliveryDate.titleLabel.text forKey:@"Bid Delivery Date"];
    [proposalDict setValue:multiprice2.text forKey:@"Multiprice2"];
    [proposalDict setValue:multiprice1.text forKey:@"Multiprice1"];
    [proposalDict setValue:note.text forKey:@"Note"];
    [proposalDict setValue:adjustedDailyCost.text forKey:@"AdjustedDailyCost"];
    [proposalDict setValue:curService.text forKey:@"Current Service"];
    [proposalDict setValue:cost.text forKey:@"Cost"];
    [proposalDict setValue:howManyPeople.text forKey:@"How Many People"];
    [proposalDict setValue:howMuchTime.text forKey:@"How Much Time"];
    [proposalDict setValue:TbanoofE.text forKey:@"NoOfEmp"];
    [proposalDict setValue:visitors.titleLabel.text forKey:@"NoOfVisitors"];
    [proposalDict setValue:rating.titleLabel.text forKey:@"Rating"];
    [proposalDict setValue:underContract.text forKey:@"Under Contract"];
    [proposalDict setValue:tillWhen.titleLabel.text forKey:@"Till When"];
    [proposalDict setValue:decisionBeMade.text forKey:@"Decision Be Made"];
    [proposalDict setValue:byWho.text forKey:@"By Who"];
    [proposalDict setValue:perWeek.text forKey:@"Per Week"];
    [proposalDict setValue:restrictedHours.text forKey:@"Restricted Hours"];
    [proposalDict setValue:priceOfItems.text forKey:@"Price Of Items"];
    [proposalDict setValue:hours.text forKey:@"Hours"];
    [proposalDict setValue:hourRate.text forKey:@"Hour Rate"];
    [proposalDict setValue:notes.text forKey:@"NotesView"];
    [proposalDict setValue:facilitybtn.titleLabel.text forKey:@"Facility Type"];
    [proposalDict setValue:[NSNumber numberWithInteger:selectedFaci] forKey:@"Selected Facility"];
    [proposalDict setValue:totalFixtures.text forKey:@"Total Fixtures"];
    [proposalDict setValue:sink.text forKey:@"Sink"];
    [proposalDict setValue:micro.text forKey:@"Microwave"];
    [proposalDict setValue:refrig.text forKey:@"Refrigerator"];
    [proposalDict setValue:vending.text forKey:@"Vending"];
    [proposalDict setValue:tables.text forKey:@"Tables"];
    [proposalDict setValue:water.text forKey:@"Water Cooler"];
    [proposalDict setValue:dish.text forKey:@"Dish Washer"];
    [proposalDict setValue:specialItems.text forKey:@"Special Items"];
    [proposalDict setValue:costOfSpecial.text forKey:@"Cost of Special"];
    [proposalDict setValue:calcLabel.text forKey:@"calcLabel"];
    [proposalDict setValue:turnBack forKey:@"Turn Back"];
    [proposalDict setValue:turnBackSent forKey:@"TurnBack Sent"];
    [proposalDict setValue:submitted forKey:@"Submitted"];
    if([billDifferent currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:@"Yes" forKey:@"BillToDifferent"];
    else
        [proposalDict setValue:@"No" forKey:@"BillToDifferent"];        
    
    if(imgData)
        [proposalDict setObject:imgData forKey:@"Image"];
    
    UIButton *btn1 = (UIButton*) [haveKeys viewWithTag:11];
    UIButton *btn2 = (UIButton*) [haveKeys viewWithTag:22];
    if([btn1 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn1.titleLabel.text forKey:@"Have Keys"];
    else if([btn2 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn2.titleLabel.text forKey:@"Have Keys"];
    
    UIButton *btn3 = (UIButton*) [dayPorter viewWithTag:11];
    UIButton *btn4 = (UIButton*) [dayPorter viewWithTag:22];
    if([btn3 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn3.titleLabel.text forKey:@"Day Porter"];
    else if([btn4 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn4.titleLabel.text forKey:@"Day Porter"];
    
    UIButton *btn5 = (UIButton*) [trashLiners viewWithTag:11];
    UIButton *btn6 = (UIButton*) [trashLiners viewWithTag:22];
    UIButton *btn7 = (UIButton*) [trashLiners viewWithTag:33];
    if([btn5 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn5.titleLabel.text forKey:@"Trash Liners"];
    else if([btn6 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn6.titleLabel.text forKey:@"Trash Liners"];
    else if([btn7 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn7.titleLabel.text forKey:@"Trash Liners"];
    
    UIButton *btn8 = (UIButton*) [bthConsumables viewWithTag:11];
    UIButton *btn9 = (UIButton*) [bthConsumables viewWithTag:22];
    UIButton *btn10 = (UIButton*) [bthConsumables viewWithTag:33];
    if([btn8 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn8.titleLabel.text forKey:@"Bath Consumables"];
    else if([btn9 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn9.titleLabel.text forKey:@"Bath Consumables"];
    else if([btn10 currentImage] == [UIImage imageNamed:@"delete.png"])
        [proposalDict setValue:btn10.titleLabel.text forKey:@"Bath Consumables"];
    
    NSMutableArray *days,*spclAreas;
    for(UIButton *btn in whichDays.subviews)
        if([btn isKindOfClass:[UIButton class]])
            if([btn currentImage] == [UIImage imageNamed:@"delete.png"]){
                if(days == nil)
                    days = [[NSMutableArray alloc] initWithObjects:btn.titleLabel.text,nil];
                else
                    [days addObject:btn.titleLabel.text];
            }
    for(UIButton *btn in specialAreas.subviews)
        if([btn isKindOfClass:[UIButton class]])
            if([btn currentImage] == [UIImage imageNamed:@"delete.png"]){
                if(spclAreas == nil)
                    spclAreas = [[NSMutableArray alloc] initWithObjects:btn.titleLabel.text,nil];
                else
                    [spclAreas addObject:btn.titleLabel.text];
            }
    if([days count]>0)
        [proposalDict setObject:days forKey:@"Which Days"];          
    if([spclAreas count]>0)
        [proposalDict setObject:spclAreas forKey:@"Special Areas"];          
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (id obj in faciltyScroller.subviews)
        if([obj isKindOfClass:[UIView class]]){
            UIView *vw = (UIView *)obj;
            if([vw.subviews count] == 8){
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                [dict setValue:tf1.text forKey:@"size1"];
                UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                [dict setValue:tf2.text forKey:@"size2"];
                UIButton *rType = (UIButton*) [vw viewWithTag:9997];
                [dict setValue:rType.titleLabel.text forKey:@"roomType"];
                UIButton *fType = (UIButton*) [vw viewWithTag:9996];
                [dict setValue:fType.titleLabel.text forKey:@"floorType"];
                UITextField *tf3 = (UITextField*) [vw viewWithTag:13000];
                [dict setValue:tf3.text forKey:@"notes"];
                UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                [dict setValue:tf4.text forKey:@"total1"];
                [arr addObject:dict];
            }
        }
    NSMutableArray *arr1 = [[NSMutableArray alloc] init];
    if([cptTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:cptTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"CPT" forKey:@"fname"];
        [dict1 setValue:cptC.text forKey:@"Cost"];
        [dict1 setValue:cptTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([vctTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:vctTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"VCT" forKey:@"fname"];
        [dict1 setValue:vctC.text forKey:@"Cost"];
        [dict1 setValue:vctTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([cerTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:cerTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"CER" forKey:@"fname"];
        [dict1 setValue:cerC.text forKey:@"Cost"];
        [dict1 setValue:cerTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([vsgTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:vsgTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"VSG" forKey:@"fname"];
        [dict1 setValue:vsgC.text forKey:@"Cost"];
        [dict1 setValue:vsgTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([crtTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:crtTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"CRT" forKey:@"fname"];
        [dict1 setValue:crtC.text forKey:@"Cost"];
        [dict1 setValue:crtTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([linTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:linTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"LIN" forKey:@"fname"];
        [dict1 setValue:linC.text forKey:@"Cost"];
        [dict1 setValue:linTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([woodTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:woodTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"WOOD" forKey:@"fname"];
        [dict1 setValue:woodC.text forKey:@"Cost"];
        [dict1 setValue:woodTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([trzTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:trzTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"TRZ" forKey:@"fname"];
        [dict1 setValue:trzC.text forKey:@"Cost"];
        [dict1 setValue:trzTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([rubTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:rubTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"RUB" forKey:@"fname"];
        [dict1 setValue:rubC.text forKey:@"Cost"];
        [dict1 setValue:rubTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }
    if([epoxyTS.text floatValue] > 0){
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc]init];
        [dict1 setValue:epoxyTS.text forKey:@"TotalSqft"];
        [dict1 setValue:@"EPOXY" forKey:@"fname"];
        [dict1 setValue:epoxyC.text forKey:@"Cost"];
        [dict1 setValue:epoxyTC.text forKey:@"TotalCost"];
        [arr1 addObject:dict1];
    }

    NSMutableArray *arr2 = [[NSMutableArray alloc] init];
    for (id obj in kitchenScroller.subviews)
        if([obj isKindOfClass:[UIView class]]){
            UIView *vw = (UIView *)obj;
            if([vw.subviews count] == 7){
                NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
                UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                [dict2 setValue:tf1.text forKey:@"size3"];
                UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                [dict2 setValue:tf2.text forKey:@"size4"];
                UIButton *btn = (UIButton*) [vw viewWithTag:9996];
                [dict2 setValue:btn.titleLabel.text forKey:@"floorType"];
                UITextField *tf = (UITextField*) [vw viewWithTag:13000];
                [dict2 setValue:tf.text forKey:@"notes"];
                UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                [dict2 setValue:tf4.text forKey:@"total2"];
                [arr2 addObject:dict2];
            }
        }
    NSMutableArray *arr3 = [[NSMutableArray alloc] init];
    for (id obj in bathroomScroller.subviews)
        if([obj isKindOfClass:[UIView class]]){
            UIView *vw = (UIView *)obj;
            if([vw.subviews count] == 9){
                NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
                UITextField *tf1 = (UITextField*) [vw viewWithTag:11000];
                [dict2 setValue:tf1.text forKey:@"size5"];
                UITextField *tf2 = (UITextField*) [vw viewWithTag:12000];
                [dict2 setValue:tf2.text forKey:@"size6"];
                UIButton *fType = (UIButton*) [vw viewWithTag:9996];
                [dict2 setValue:fType.titleLabel.text forKey:@"floorType"];
                UITextField *tf5 = (UITextField*) [vw viewWithTag:15000];
                [dict2 setValue:tf5.text forKey:@"fixtures"];
                UITextField *tf = (UITextField*) [vw viewWithTag:13000];
                [dict2 setValue:tf.text forKey:@"notes1"];
                UILabel *tf4 = (UILabel*) [vw viewWithTag:14000];
                tf4.text = [NSString stringWithFormat:@"%.2f",([tf1.text floatValue] * [tf2.text floatValue])];
                [dict2 setValue:tf4.text forKey:@"total3"];
                [arr3 addObject:dict2];
            }
        }
    [proposalDict setObject:arr forKey:@"FacilityType"];
    [proposalDict setObject:arr1 forKey:@"fTypes"];
    [proposalDict setObject:arr2 forKey:@"Kitchens"];    
    [proposalDict setObject:arr3 forKey:@"Bathrooms"];    
    
    CGPoint offSet = scroller.contentOffset;
    NSMutableData *pdfData = [[NSMutableData alloc] init];
    CGDataConsumerRef pdfConsumer = CGDataConsumerCreateWithCFData((__bridge CFMutableDataRef)pdfData);
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(UIInterfaceOrientationIsPortrait(orientation)){
        CGRect mediaBox = CGRectMake(0, 0, 768, 1024);
       
        CGContextRef pdfContext = CGPDFContextCreate(pdfConsumer, &mediaBox, NULL);
        
        [scroller setContentOffset:CGPointMake(0,0) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image1 CGImage]);
        CGContextEndPage(pdfContext);
        
        [scroller setContentOffset:CGPointMake(0,922) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image2 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image2 CGImage]);
        CGContextEndPage(pdfContext);
        
        [scroller setContentOffset:CGPointMake(0,1840) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image3 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image3 CGImage]);
        CGContextEndPage(pdfContext);
        
        [scroller setContentOffset:CGPointMake(0,2670) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image4 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image4 CGImage]);
        CGContextEndPage(pdfContext);
        
        CGContextRelease(pdfContext);
    }else{
        CGRect mediaBox = CGRectMake(0, 0, 1024, 768);
        
        CGContextRef pdfContext = CGPDFContextCreate(pdfConsumer, &mediaBox, NULL);
        
        [scroller setContentOffset:CGPointMake(0,0) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image1 CGImage]);
        CGContextEndPage(pdfContext);
        
        [scroller setContentOffset:CGPointMake(0,660) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image2 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image2 CGImage]);
        CGContextEndPage(pdfContext);
        
        [scroller setContentOffset:CGPointMake(0,1320) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image3 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image3 CGImage]);
        CGContextEndPage(pdfContext);
        
        [scroller setContentOffset:CGPointMake(0,1995) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image4 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image4 CGImage]);
        CGContextEndPage(pdfContext);

        [scroller setContentOffset:CGPointMake(0,2678) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image5 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image5 CGImage]);
        CGContextEndPage(pdfContext);

        [scroller setContentOffset:CGPointMake(0,3338) animated:NO];
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image6 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CGContextBeginPage(pdfContext, &mediaBox);
        CGContextDrawImage(pdfContext, mediaBox, [image6 CGImage]);
        CGContextEndPage(pdfContext);

        CGContextRelease(pdfContext);
    }
    CGDataConsumerRelease(pdfConsumer);
    [proposalDict setObject:pdfData forKey:@"pdfData"];
    
    [scroller setContentOffset:offSet animated:NO];
    
    if (cellIndex>-1) {
       AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
       [appDelegate updateRecord:proposalDict atIndex:cellIndex];
    }
    if(delegate)
        [delegate reloadTable];
}

-(void)calSaveBtnPressed{
     [self calculateBtn];
}

-(IBAction)scrollToBottom:(id)sender{
    [scroller setContentOffset:CGPointMake(0,2900) animated:YES];
}

-(IBAction)datePickerBtn:(id)sender{
    selectedBtn = (UIButton*)sender;
    
    if(self.datePopOverController == nil){
        UIViewController* popoverContent = [[UIViewController alloc] init];
        UIView* popoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 260)];
        popoverView.backgroundColor = [UIColor whiteColor];
 
        dobPicker = [[UIDatePicker alloc] init];
        dobPicker.frame=CGRectMake(0, 44, 320, 216);
        [dobPicker setDate:[NSDate date]];
        
        UIToolbar *toolbar = [[UIToolbar alloc] init];
        toolbar.barStyle = UIBarStyleBlackOpaque;
        toolbar.frame = CGRectMake(0, 0, 320, 44);
        
        UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
        
        NSArray *items = [NSArray arrayWithObjects:cancel, flexiableItem, done, nil];
        toolbar.items = items;

        [popoverView addSubview:toolbar];
        [popoverView addSubview:dobPicker];
        popoverContent.view = popoverView;
        
        self.datePopOverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
        [self.datePopOverController setPopoverContentSize:CGSizeMake(320, 260) animated:NO];
    }
    [dobPicker setDate:[NSDate date]];

    dateFormat = [[NSDateFormatter alloc] init];
    if(selectedBtn.tag == 100){
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        dobPicker.datePickerMode = UIDatePickerModeDate; 
    }
    else{
        [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm aa"];
        dobPicker.minuteInterval = 15;
        dobPicker.datePickerMode = UIDatePickerModeDateAndTime;
    }
    [self.datePopOverController presentPopoverFromRect:selectedBtn.bounds inView:selectedBtn permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)cancel{
    if(self.datePopOverController)
        [self.datePopOverController dismissPopoverAnimated:YES];
}

-(void)done{
    if(self.datePopOverController)
        [self.datePopOverController dismissPopoverAnimated:YES];
    
    if([dateFormat stringFromDate:[dobPicker date]])
        [selectedBtn setTitle:[dateFormat stringFromDate:[dobPicker date]] forState:UIControlStateNormal];
}
//-(void)valueChanged:(id)sender{
//    dbPicker = (UIDatePicker*)sender;
//}

-(IBAction)backBtnClicked:(id)sender{
    [self calculateBtn];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)TableAlertButton:(UIButton *)sender{
    Tba=(UIButton *)sender;
    
    if(self.tableViewPopOver == nil){
        self.tableViewPopOver = [[TableViewPopOver alloc] initWithStyle:UITableViewStylePlain];
        self.tableViewPopOver.delegate = self;
    }
    if([self.tableViewPopOver.list count] > 0){
        [self.tableViewPopOver.list removeAllObjects];
    }
    if(self.popOverController == nil){
        self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.tableViewPopOver];
    }

    if(Tba.tag==9994){
        self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil];
        self.tableViewPopOver.sectionTitle = @"Rating";
    }else if(Tba.tag==9995){
        self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"Low",@"Medium",@"High",nil];
        self.tableViewPopOver.sectionTitle = @"No Of Employees";
    }else if(Tba.tag==9999){
        self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"Round Up",@"Minimum Price",@"Location",@"Budget",@"Building",@"Density Space",@"1x Job",nil];
        self.tableViewPopOver.sectionTitle = @"Reason for Adjustment";
    }else if(Tba.tag==9998){
        self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General Office",@"Multi-Tenant",@"Medical/Dental",@"Schools",@"Financial Institutions",@"Places of Worship",@"Auto Dealers",@"Apartment/Condos",@"Health Club",@"Restaurant",@"Movie Theater",@"Bowling Alley",@"Retail Store",@"Library",nil];
        self.tableViewPopOver.sectionTitle = @"Facility Type";
    }else if(Tba.tag==9997){
        if(selectedFaci==0)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==1)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Suite 1",@"Suite 2",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==2)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Exam room",@"Treatment",@"Isolation",@"Water room",@"Lab",@"Records Room",@"File area",@"Storage",@"Copy area",@"Operatory",@"X-Ray/MRI/CT",@"Elevator",nil];
        else if(selectedFaci==3 || selectedFaci==5)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Classrooms",@"Sanctuary",@"Gymnasium",@"Multi-purpose",@"Locker room",@"Library",@"Computer Lab",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==4)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Teller area",@"Drive up",@"Coupon room",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==6)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Showroom",@"Service department",@"Service drive",@"Service writers",@"Finance",@"Cashier",@"Car wash",@"Body shop",@"Accessory center",@"Parts department",@"Waiting area",@"Cafe",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==7)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Pool house",@"Laundry",@"Trash chute",@"Locker room",@"Meeting room",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==8)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Track",@"Gymnasium",@"Sauna",@"Whirlpool",@"Pool deck",@"Child care",@"Yoga/Dance",@"Cycling",@"Fitness",@"Cafe",@"Locker",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==9)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Dining room",@"Outdoor dining",@"Bar",@"Kitchen",@"Food prep area",@"Private Dining",@"Party room",@"Storage areas ",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==10)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Concession Area",@"Storage",@"Auditorium",@"Game Area",@"Ticket booth",@"Party room",@"File area",@"Storage", @"Copy area",@"Elevator",nil];
        else if(selectedFaci==11)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Approach/scoring area",@"Concourse",@"Bar",@"Party room",@"Game Room",@"Storage area",@"Lockers",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==12)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Customer service",@"Cashier",@"Retail floor",@"File area",@"Storage",@"Copy area",@"Elevator",nil];
        else if(selectedFaci==13)
            self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"General office",@"Common office",@"Executive office",@"Hallway",@"Conference room",@"Reception",@"Waiting room",@"Lobby",@"Foyer",@"Fitness center",@"Warehouse",@"Stairways",@"Landings",@"Adult Library",@"Youth Library",@"Audio-Visual",@"Computer lab",@"Checkout/Book Return",@"Elevator",nil];
        
        self.tableViewPopOver.sectionTitle = @"Room Type";
    }else if(Tba.tag==9996){
        self.tableViewPopOver.list = [NSMutableArray arrayWithObjects:@"CPT",@"VCT",@"CER",@"VSG",@"CRT",@"LIN",@"WOOD",@"TRZ",@"RUB",@"EPOXY",nil];
        self.tableViewPopOver.sectionTitle = @"Flooring Type";
    }
    [self.tableViewPopOver.tableView setContentOffset:CGPointZero animated:YES];
    [self.popOverController presentPopoverFromRect:Tba.bounds inView:Tba permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)radioButton:(id)sender{
    UIButton *button = (UIButton*)sender;
    [button setSelected:YES];
    if([button superview] == trashLiners || [button superview] == bthConsumables){
        for(UIView *vw in [button superview].subviews)
            if([vw isKindOfClass:[UIButton class]]){
                UIButton *btn = (UIButton*)vw;
                if(btn.tag == button.tag){
                    [btn setSelected:NO];
                    [btn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
                }else
                    [btn setImage:[UIImage imageNamed:@"submit-bt.png"] forState:UIControlStateNormal];
            }
        return;
    }
    if(button.tag==11){
        [button setSelected:NO];
        [button setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        UIButton *btn = (UIButton*)[[button superview] viewWithTag:22];
        [btn setImage:[UIImage imageNamed:@"submit-bt.png"] forState:UIControlStateNormal];
    }else{
        [button setSelected:NO];
        [button setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        UIButton *btn = (UIButton*) [[button superview] viewWithTag:11];
        [btn setImage:[UIImage imageNamed:@"submit-bt.png"] forState:UIControlStateNormal];
    }
    if([button superview] == dayPorter || [button superview] == haveKeys)
        [self calcMonthlyQuote];
}

-(void)checkBoxButton:(id)sender{
    UIButton *btn = (UIButton*)sender;
    if([btn isSelected]){
        [btn setImage:[UIImage imageNamed:@"submit-bt.png"] forState:UIControlStateNormal];
        [btn setSelected:NO];   
    }
    else{
        [btn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        [btn setSelected:YES];
    }
}

-(void)selectedRow:(NSInteger)index{
    if(self.popOverController!=nil)
		[self.popOverController dismissPopoverAnimated:YES];

    if(Tba.tag==9998){
        selectedFaci=index;
        [facilitybtn setTitle:[NSString stringWithFormat:@"%@",[self.tableViewPopOver.list objectAtIndex:index]] forState:UIControlStateNormal];
    }else
        [Tba setTitle:[NSString stringWithFormat:@"%@",[self.tableViewPopOver.list objectAtIndex:index]] forState:UIControlStateNormal];

}

#pragma mark - Autorotate methods

-(BOOL)shouldAutorotate{
    return YES;
}

@end