//
//  ViewController.m
//  ImageOne
//
//  Created by RAMANUJAN SAMPATHKUMARAN on 2/20/12.
//  Copyright (c) 2012 Saint Xvier University. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@implementation ViewController
@synthesize tableView,identifier,list;

-(IBAction)startNewProposal:(id)sender
{   
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"",@"Company",@"",@"Image",@"",@"pdfData",@"",@"Identifier",@"",@"Bid Delivery Date",@"",@"Submitted",@"",@"TurnBack Sent",@"",@"Turn Back", nil];
    if(list)
        [list addObject:dict];
    else
        list = [[NSMutableArray alloc] initWithObjects:dict, nil];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate insertRecord:dict atIndex:[list count]-1];
    
    ProposalViewController *nextViewController= [[ProposalViewController alloc ] initWithNibName:@"ProposalViewController" bundle:nil];
    nextViewController.delegate =self;
    nextViewController.cellIndex=[list count]-1;
    [self.navigationController pushViewController:nextViewController animated:YES];
}

- (void)submitBtnClicked:(id)sender{
    submitBtn = (UIButton*)sender;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSDictionary *dict = [appDelegate decodeData:[list objectAtIndex:submitBtn.tag]];

    if([[dict valueForKey:@"Identifier"] length] == 0){
        UIAlertView *identiferAlrt = [[UIAlertView alloc] initWithTitle:@"Image One" message:@"An identifier # hasn't been entered, would you like to submit anyway?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
        identiferAlrt.tag = 3;
        [identiferAlrt show];
        return;
    }
    [self sendEmail];
}

-(void)sendEmail{
    //Email
    if(![MFMailComposeViewController canSendMail]) {
        UIAlertView *errAlrtView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email is not configured" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errAlrtView show];
        return;
    }
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableDictionary *proposalDict = [appDelegate decodeData:[list objectAtIndex:submitBtn.tag]];

    NSMutableString *body = [[NSMutableString alloc]init];
    identifier = [appDelegate decodeData:[list objectAtIndex:submitBtn.tag]];
    
    submitController = [[MFMailComposeViewController alloc] init];
    submitController.mailComposeDelegate = self;
    if([[proposalDict valueForKey:@"Company"] length] > 0)
        [submitController setSubject:[NSString stringWithFormat:@"\"Bid Delivered\" / %@ # %@",[proposalDict valueForKey:@"Company"],[identifier valueForKey:@"Identifier"] ]];
    else
        [submitController setSubject:[NSString stringWithFormat:@"\"Bid Delivered\" / %@",[proposalDict valueForKey:@"Identifier"]]];
    
    if([proposalDict objectForKey:@"Image"])
        [submitController addAttachmentData:[proposalDict objectForKey:@"Image"] mimeType:@"image/jpeg" fileName:@"Image.jpeg"];
    [submitController addAttachmentData:[proposalDict objectForKey:@"pdfData"] mimeType:@"application/pdf" fileName:@"Measurement.pdf"];
    
    [body appendString:@"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>"];
    
    if([[proposalDict valueForKey:@"FacilityType"] count] > 0){
        [body appendString:@"<b>Facility Type :</b><br><br><table border=\"1\"><tr><th>Office Size</th><th>Room Type</th><th>Flooring Type</th><th>Notes</th><th>Total Sq ft.</th></tr>"];
        
        for(NSDictionary *dict in [proposalDict valueForKey:@"FacilityType"]){
            [body appendString:[NSString stringWithFormat:@"<tr><td align=\"center\">%@ x %@</td>",[dict valueForKey:@"size1"],[dict valueForKey:@"size2"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"roomType"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"floorType"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"notes"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"total1"]]];
            [body appendString:@"</tr>"];
        }
        [body appendString:@"</table><br><br>"];
    }
    
    if([[proposalDict valueForKey:@"Kitchens"] count] > 0){
        [body appendString:@"<b>Kitchen :</b><br><br><table border=\"1\"><tr><th>Room Size</th><th>Flooring Type</th><th>Notes</th><th>Total Sq ft.</th></tr>"];
        for(NSDictionary *dict in [proposalDict valueForKey:@"Kitchens"]){
            [body appendString:[NSString stringWithFormat:@"<tr><td align=\"center\">%@ x %@</td>",[dict valueForKey:@"size3"],[dict valueForKey:@"size4"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"floorType"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"notes"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"total2"]]];
            [body appendString:@"</tr>"];
        }
        [body appendString:@"</table><br><br>"];
    }
    
    if([[proposalDict valueForKey:@"Bathrooms"] count] > 0){
        [body appendString:@"<b>Bathroom :</b><br><br><table border=\"1\"><tr><th>Room Size</th><th>Flooring Type</th><th>Fixtures</th><th>Notes</th><th>Total Sq ft.</th></tr>"];
        for(NSDictionary *dict in [proposalDict valueForKey:@"Bathrooms"]){
            [body appendString:[NSString stringWithFormat:@"<tr><td align=\"center\">%@ x %@</td>",[dict valueForKey:@"size5"],[dict valueForKey:@"size6"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"floorType"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"fixtures"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"notes1"]]];
            [body appendString:[NSString stringWithFormat:@"<td align=\"center\">%@</td>",[dict valueForKey:@"total3"]]];
            [body appendString:@"</tr>"];
        }
        [body appendString:@"</table>"];
    }
    
    [body appendString:@"</body></html>"];
    [submitController setMessageBody:body isHTML:YES];
    [self presentModalViewController:submitController animated:YES];
}

- (void)turnBackClicked:(id)sender{
    turnBackBtn=(UIButton*)sender;
    
    if(self.tableViewPopOver == nil){
        self.tableViewPopOver = [[TableViewPopOver alloc] initWithStyle:UITableViewStylePlain];
        self.tableViewPopOver.delegate = self;
        self.tableViewPopOver.list = tbReason;
        self.tableViewPopOver.sectionTitle = @"Reason for Turn back";
    }
    if(self.popOverController == nil){
        self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.tableViewPopOver];
    }
    
    [self.popOverController presentPopoverFromRect:turnBackBtn.bounds inView:turnBackBtn permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)deleteClicked:(id)sender{
    deleteBtn = (UIButton*)sender;
    
    UIAlertView *deleteAlrt = [[UIAlertView alloc] initWithTitle:@"Image One" message:@"Do you want to delete this Proposal?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete",nil];
    deleteAlrt.tag = 2;
    [deleteAlrt show];
}

-(void)selectedProposal:(id)sender {
    UIButton *selectedBtn=(UIButton*)sender;
    ProposalViewController *nextViewController= [[ProposalViewController alloc ] initWithNibName:@"ProposalViewController" bundle:nil];
    nextViewController.delegate = self;
    nextViewController.cellIndex=selectedBtn.tag;
    [self.navigationController pushViewController:nextViewController animated:YES];
}

-(void)reloadTable{
    if([list count] > 0){
        [list removeAllObjects];
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [list addObjectsFromArray:[appDelegate getAllRecords]];
        [tableView reloadData];
    }
}

#pragma mark -
#pragma mark MFMailCompose delegate

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if(result == MFMailComposeResultSent){
        if(controller == submitController){
            //[submitBtn setSelected:TRUE];
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            NSMutableDictionary *dict = [appDelegate decodeData:[list objectAtIndex:submitBtn.tag]];

            [dict setValue:@"YES" forKey:@"Submitted"];
            [appDelegate updateRecord:dict atIndex:submitBtn.tag];
            
            [self.tableView reloadData];
        }
        else{
            [turnBackBtn setSelected:TRUE];
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            NSMutableDictionary *dict = [appDelegate decodeData:[list objectAtIndex:turnBackBtn.tag]];

            [dict setValue:@"YES" forKey:@"TurnBack Sent"];
            [appDelegate updateRecord:dict atIndex:submitBtn.tag];
            
            [self.tableView reloadData];
        }
    }
    [self dismissModalViewControllerAnimated:YES];   
}

#pragma mark -
#pragma mark tableView dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [list count];
}

- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier cellForRowAtIndexPath:(NSIndexPath *)indexPath{

	CGRect cellFrame = CGRectMake(5,0,758,39);	
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //background cell changed here
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.frame = cellFrame;
	
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSDictionary *diccell = [appDelegate decodeData:[list objectAtIndex:indexPath.row]];

    selected = [UIButton buttonWithType:UIButtonTypeCustom];
	selected.frame = CGRectMake(10,2,300,36);
	[selected setImage:[UIImage imageNamed:@"bar1.png"] forState:UIControlStateNormal];
	selected.tag = indexPath.row;
   	[selected addTarget:self action:@selector(selectedProposal:) forControlEvents:UIControlEventTouchUpInside];
	[cell addSubview:selected];
    
    UILabel *txtLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(20,3,270,34)];
	txtLabel1.textColor = [UIColor whiteColor];
	txtLabel1.backgroundColor = [UIColor clearColor];
    txtLabel1.textAlignment = UITextAlignmentCenter;
	txtLabel1.font = [UIFont systemFontOfSize:18];
	txtLabel1.tag = 1;
    txtLabel1.text=[NSString stringWithFormat:@"%@",[diccell valueForKey:@"Company"]];
	[cell addSubview:txtLabel1];
    
    CGRect bgFrame2 = CGRectMake(330,2,120,36);
    UIImageView *LblbackView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"date-bg.png"]];
	LblbackView.frame = bgFrame2;
	[cell addSubview:LblbackView];
	
	UILabel *txtLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(338,3,110,32)];
	txtLabel2.textColor = [UIColor whiteColor];
	txtLabel2.backgroundColor = [UIColor clearColor];
    txtLabel2.textAlignment = UITextAlignmentCenter;
	txtLabel2.font = [UIFont systemFontOfSize:18];
	txtLabel2.tag = 2;
    if([[diccell valueForKey:@"Bid Delivery Date"] length] > 10)
        txtLabel2.text = [NSString stringWithFormat:@"%@",[[diccell valueForKey:@"Bid Delivery Date"] substringToIndex:11]];
	[cell addSubview:txtLabel2];
    
    submit = [UIButton buttonWithType:UIButtonTypeCustom];
	submit.frame = CGRectMake(465, 2, 100,36);
    if([[diccell valueForKey:@"Submitted"] isEqualToString:@"YES"])
            [submit setBackgroundImage:[UIImage imageNamed:@"delete1.png"] forState:UIControlStateNormal];    
    else
        [submit setBackgroundImage:[UIImage imageNamed:@"submit-bt1.png"] forState:UIControlStateNormal];
    [submit setTitle:@"Submit" forState:UIControlStateNormal];
    submit.titleLabel.font = [UIFont systemFontOfSize:16];
	submit.tag = indexPath.row;
	[submit addTarget:self action:@selector(submitBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
	[cell addSubview:submit];
    
    if([[diccell valueForKey:@"TurnBack Sent"] isEqualToString:@"YES"]){
        submit.frame = CGRectMake(490, 2, 100,36);

        UIButton *delete = [UIButton buttonWithType:UIButtonTypeCustom];
        delete.frame = CGRectMake(610,2,100,36);
        [delete setBackgroundImage:[UIImage imageNamed:@"submit-bt1.png"] forState:UIControlStateNormal];
        [delete setTitle:@"Delete" forState:UIControlStateNormal];
        delete.titleLabel.font = [UIFont systemFontOfSize:16];
        delete.tag = indexPath.row;
        [delete addTarget:self action:@selector(deleteClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:delete];
    }else{
        turnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        turnBack.frame = CGRectMake(580, 2, 160,36);
        [turnBack setBackgroundImage:[UIImage imageNamed:@"submit-bt1.png"] forState:UIControlStateNormal];
        [turnBack setBackgroundImage:[UIImage imageNamed:@"delete1.png"] forState:UIControlStateSelected];
        if([diccell valueForKey:@"Turn Back"])
            [turnBack setTitle:[diccell valueForKey:@"Turn Back"] forState:UIControlStateNormal];
        else
            [turnBack setTitle:@"Turn back" forState:UIControlStateNormal];
        turnBack.titleLabel.font = [UIFont systemFontOfSize:16];
        turnBack.tag = indexPath.row;
        [turnBack addTarget:self action:@selector(turnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:turnBack];
    }
	return cell;
}	

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell;
	static NSString *kDisplayCell_ID = @"DisplayCellID";
	cell = [tableView1 dequeueReusableCellWithIdentifier:kDisplayCell_ID];
	
	cell = [self getCellContentView:kDisplayCell_ID cellForRowAtIndexPath:indexPath];
	
	return cell;		
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50;
}

-(void)selectedRow:(NSInteger)index{
    if(self.popOverController!=nil)
		[self.popOverController dismissPopoverAnimated:YES];

    [turnBackBtn setTitle:[tbReason objectAtIndex:index] forState:UIControlStateNormal];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSDictionary *dict = [appDelegate decodeData:[list objectAtIndex:turnBackBtn.tag]];
    
    [dict setValue:[tbReason objectAtIndex:index] forKey:@"Turn Back"];
    [appDelegate updateRecord:dict atIndex:turnBackBtn.tag];
    
    UIAlertView *sendAlrt = [[UIAlertView alloc] initWithTitle:@"Image One" message:@"Do you want to send this Proposal?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
    sendAlrt.tag = 1;
    [sendAlrt show];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tbView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.05 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            if([appDelegate deleteRecordAtIndex:indexPath.row]){
                [list removeObjectAtIndex:indexPath.row];
                [self.tableView reloadData];
            }
        });
    }
}

#pragma mark -
#pragma mark Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1 && buttonIndex == 1){
        turnBackController = [[MFMailComposeViewController alloc] init];
        turnBackController.mailComposeDelegate = self;
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        identifier = [appDelegate decodeData:[list objectAtIndex:turnBackBtn.tag]];
        
        if([[identifier valueForKey:@"Company"] length] > 0)
            [turnBackController setSubject:[NSString stringWithFormat:@"\"Turn back\" / %@ # %@ ",[identifier valueForKey:@"Company"], [identifier valueForKey:@"Identifier"]]];
        else
            [turnBackController setSubject:[NSString stringWithFormat:@"\"Turn back\" / %@",[identifier valueForKey:@"Identifier"]]];

        [turnBackController setMessageBody:turnBackBtn.titleLabel.text isHTML:YES];
        [self presentModalViewController:turnBackController animated:YES];
    }else if(alertView.tag == 2 && buttonIndex == 1){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.05 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            if([appDelegate deleteRecordAtIndex:deleteBtn.tag]){
                [list removeObjectAtIndex:deleteBtn.tag];
                [self.tableView reloadData];
            }
        });
    }else if(alertView.tag == 3 && buttonIndex == 1)
        [self sendEmail];
}

#pragma mark - View lifecycle




- (void)viewDidLoad{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    list=[[NSMutableArray alloc] initWithArray:[appDelegate getAllRecords]];
    
    self.navigationController.navigationBar.hidden=TRUE;
    self.navigationItem.hidesBackButton = TRUE;

    
    tbReason = [[NSMutableArray alloc] initWithObjects:@"No Bid",@"Kept current service",@"Choose competitor",@"Sold", nil];
    versionNo.text = [NSString stringWithFormat:@"Version : %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        [self.view bringSubviewToFront:self.profileBtn];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    [tableView setEditing:editing animated:animated];
}


- (IBAction)profile:(id)sender
{
    MainViewController *nextViewController= [[MainViewController alloc ]initWithNibName:@"MainViewController" bundle:nil];
    [self.navigationController pushViewController:nextViewController animated:YES];
}

#pragma mark - Autorotate methods

-(BOOL)shouldAutorotate{
    return YES;
}

@end