//
//  IOSRequest.h
//  ImageOne
//
//  Created by Vensi Developer on 11/25/13.
//
//

#import <Foundation/Foundation.h>
#import "NSString+WebService.h"

typedef void(^RequestCompletionHandler)(NSString*,NSError*);
typedef void(^RequestDictionaryCompletionHandler) (NSDictionary*);

@interface IOSRequest : NSObject
+(void)requestToPath:(NSString *)path onCompletion:(RequestCompletionHandler)complete;

+(void) loginWithUsername: (NSString *)userName
              andPassword: (NSString *)password
             oncompletion:(RequestDictionaryCompletionHandler)complete;
@end
