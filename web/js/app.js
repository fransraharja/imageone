var app = angular.module('login',[]).config(function($routeProvider){
	this.store = new Persist.Store('Imageone Store');
    $routeProvider.
     when('/login',{
     templateUrl:'template/login.html',
     controller: 'loginController'
     });
    $routeProvider.
	    when('/home',{			
        templateUrl:'template/home.html',
        controller: 'homeController'
	});
	
	$routeProvider.
	    when('/franchise',{			
        templateUrl:'template/franchise.html',
        controller: 'franchiseController'
	});	
	$routeProvider.
	    when('/addFranchise',{			
        templateUrl:'template/addfranchise.html',
        controller: 'addfranchiseController'
	});
	$routeProvider.
	    when('/editFranchise',{			
        templateUrl:'template/editFranchise.html',
        controller: 'editfranchiseController'
	});	

	$routeProvider.
	    when('/customer',{			
        templateUrl:'template/customer.html',
        controller: 'customerController'
	});	
	$routeProvider.
	    when('/addcustomer',{			
        templateUrl:'template/addcustomer.html',
        controller: 'addcustomerController'
	});
	$routeProvider.
	    when('/editcustomer',{			
        templateUrl:'template/editcustomer.html',
        controller: 'editcustomerController'
	});	
		
	$routeProvider.
	    when('/changePassword',{			
        templateUrl:'template/changepassword.html',
        controller: 'changepasswordController'
	});
	$routeProvider.
	    when('/logout',{			
        templateUrl:'template/logout.html',
        controller: 'logoutController'
	});		
	 
    $routeProvider.otherwise({redirectTo: '/login'})
});
