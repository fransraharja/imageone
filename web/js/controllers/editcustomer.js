app.controller('editcustomerController',function($scope,$location,$http){

	if(store.get('secrectKey') == null || store.get('secrectKey') == '')

	{

		$location.path('/login');

	}

	else

	{

	 $http.post('webservices/userLogin.php', {'secrectKey': store.get('secrectKey')}).success(function(data, status, headers, config) 

	 {	 			

			if(data ==0 && store.get('secrectKey') !== data)

			{

				store.remove('secrectKey');			

				$location.path('/login');

			} 

			else

			{				

				$scope.setRoute = function(route){

		        $location.path(route);

	            }

				$scope.logout = function(){	

				$location.path('/logout');

			   }

			    $scope.type=data.UserDetails[0].type;

				$scope.menu=data.UserDetails[0].menu;

				if($scope.type == 'f' && $scope.menu == 0)

				{

					$location.path('/home');			

				}

				else

				{		   

			    $scope.errors = [];

                $scope.msgs = [];
				
				var data= JSON.parse(store.get("currentcustomer"));
				console.log(data);		
				var term=data.term;
				var businesstype=data.businesstype;
				var represent=data.represent;
				document.getElementById('firstname').value=data.firstname;
				document.getElementById('lastname').value=data.lastname;
				document.getElementById('term').value=data.term;
				document.getElementById('namechecks').value=data.namechecks;
     			document.getElementById('businesstype').value=data.businesstype;
				document.getElementById('represent').value=data.represent;
				document.getElementById('address').value=data.address;
				document.getElementById('city').value=data.city;
				document.getElementById('state').value=data.state;
				document.getElementById('country').value=data.country;
				document.getElementById('mphone').value=data.mphone;
                document.getElementById('hidmail').value=data.email;
				document.getElementById('zip').value=data.zip;
				document.getElementById('fax').value=data.fax;
				
                $scope.updatecustomer = function() {
				
				var hidmailid=document.getElementById('hidmail').value;
				
				document.getElementById('er1').innerHTML = '';

				document.getElementById('er2').innerHTML = '';

				document.getElementById('er3').innerHTML = '';

				document.getElementById('er4').innerHTML = '';

				document.getElementById('er5').innerHTML = '';

				document.getElementById('er6').innerHTML = '';

				document.getElementById('er7').innerHTML = '';

				document.getElementById('er8').innerHTML = '';

				document.getElementById('er9').innerHTML = '';

				document.getElementById('er10').innerHTML = '';

			//	document.getElementById('er11').innerHTML = '';						

				//var x = document.getElementById('firstname').value; 
				var firstname = document.getElementById('firstname').value; 

				if(firstname == '' || firstname== null)

			    {

				   document.getElementById('er1').innerHTML = '* Enter Firstname';				  

				   document.getElementById('firstname').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z ]+$/;  

					if(!numbers.test(firstname))  

					{

					document.getElementById('er1').innerHTML="* Enter Only Alphabets";

					document.getElementById('firstname').focus();

			    	return false;

			    	}			

				}

				//var x = document.getElementById('lastname').value; 
				var lastname = document.getElementById('lastname').value; 

				if(lastname == '' || lastname == null)

			    {

				   document.getElementById('er2').innerHTML = '* Enter Lastname';				   

				   document.getElementById('lastname').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z ]+$/;  

					if(!numbers.test(lastname))  

					{

					document.getElementById('er2').innerHTML="* Enter Only Alphabets";					

					document.getElementById('lastname').focus();

			    	return false;

			    	}			

				}

				//var x = document.getElementById('namechecks').value;
				var namechecks = document.getElementById('namechecks').value; 

				if(namechecks == '' || namechecks == null)

			    {

				   document.getElementById('er3').innerHTML = '* Enter Name on Checks';				  

				   document.getElementById('namechecks').focus();


				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z ]+$/;  

					if(!numbers.test(namechecks))  

					{

					document.getElementById('er3').innerHTML="* Enter Only Alphabets";					

					document.getElementById('namechecks').focus();

			    	return false;

			    	}			

				}				

				//var x = document.getElementById('address').value; 
				var address = document.getElementById('address').value; 

				if(address == '' || address == null)

			    {

				   document.getElementById('er4').innerHTML = '* Enter Address';				  

				   document.getElementById('address').focus();

				   return false;

			    }

				var city = document.getElementById('city').value; 

				if(city == '' || city == null)

			    {

				   document.getElementById('er5').innerHTML = '* Enter City';				  

				   document.getElementById('city').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z ]+$/;  

					if(!numbers.test(city))  

					{

					document.getElementById('er5').innerHTML="* Enter Only Alphabets";					

					document.getElementById('city').focus();

			    	return false;

			    	}			

				}

				var state = document.getElementById('state').value; 

				if(state == '' || state == null)

			    {

				   document.getElementById('er6').innerHTML = '* Enter State';				  

				   document.getElementById('state').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z ]+$/;  

					if(!numbers.test(state))  

					{

					document.getElementById('er6').innerHTML="* Enter Only Alphabets";					

					document.getElementById('state').focus();

			    	return false;

			    	}			

				}				

	           var zip = document.getElementById('zip').value;

				if(zip=='' || zip==null)

				{			

					document.getElementById('er7').innerHTML="* Enter Zip";

					document.getElementById('zip').focus();

			    	return false;

				}

				else

				{	

			    	var numbers = /^[0-9]{5}$/; 			

					if(!numbers.test(zip))			

					{			

					document.getElementById('er7').innerHTML="* Enter Valid Zip Code";			

					document.getElementById('zip').focus();			

					return false;			

					}			

				}

				var country = document.getElementById('country').value; 

				if(country == '' || country == null)

			    {

				   document.getElementById('er8').innerHTML = '* Enter Country';				  

				   document.getElementById('country').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z ]+$/;  

					if(!numbers.test(country))  

					{

					document.getElementById('er8').innerHTML="* Enter Only Alphabets";					

					document.getElementById('country').focus();

			    	return false;

			    	}			

				}				

				var mphone = document.getElementById('mphone').value;

				if(mphone=='' || mphone==null)

				{			

					document.getElementById('er9').innerHTML="* Enter Mobile Number";

					document.getElementById('mphone').focus();

			    	return false;

				}

				else

				{	

			    	var numbers = /^[0-9]{10}$/; 			

					if(!numbers.test(mphone))			

					{			

					document.getElementById('er9').innerHTML="* Enter Valid Number";			

					document.getElementById('mphone').focus();			

					return false;			

					}			

				}

				var fax = document.getElementById('fax').value;

				if(fax=='' || fax==null)

				{			

					document.getElementById('er10').innerHTML="* Enter Fax";

					document.getElementById('fax').focus();

			    	return false;

				}

				


                    $scope.errors.splice(0, $scope.errors.length); 

                    $scope.msgs.splice(0, $scope.msgs.length);
					


                   $http.post('webservices/editcustomers.php', {'secrectKey':store.get('secrectKey'),'firstname': firstname, 'lastname': lastname, 'term': term,'namecheck': namechecks,'businesstype': businesstype, 'represent': represent, 'address': address, 'city': city, 'state': state, 'zip': zip, 'country': country, 'mphone': mphone, 'fax': fax , 'email': hidmailid}


                    ).success(function(data, status, headers, config) {

                        if (data.msg != '')

                        {

                            $scope.msgs.push(data.msg);

                        }

                        else

                        {

                            $scope.errors.push(data.error);

                        }

						if(data == 0)

						{

							store.remove('secrectKey');			

				            $location.path('/login');

						}

                    }).error(function(data, status) { 

                        $scope.errors.push(status);

                    });

                } 

			}

				

			}

			

	  });

   }

});