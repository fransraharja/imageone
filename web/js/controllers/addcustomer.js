app.controller('addcustomerController',function($scope,$location,$http){

	if(store.get('secrectKey') == null || store.get('secrectKey') == '')

	{

		$location.path('/login');

	}

	else

	{

	 $http.post('webservices/userLogin.php', {'secrectKey': store.get('secrectKey')}).success(function(data, status, headers, config) 

	 {	 			

			if(data ==0 && store.get('secrectKey') !== data)

			{

				store.remove('secrectKey');			

				$location.path('/login');

			} 

			else

			{				

				$scope.setRoute = function(route){

		        $location.path(route);

	            }

				$scope.logout = function(){	

				$location.path('/logout');

			   }

			   		   

			    $scope.errors = [];

                $scope.msgs = [];

                $scope.customer = function() {

				var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

	            var string_length = 15;

	            var randomstring = '';

	            for (var i=0; i<string_length; i++) {

		        var rnum = Math.floor(Math.random() * chars.length);

		        randomstring += chars.substring(rnum,rnum+1);

	           }

			    var password  = randomstring;

				document.getElementById('er1').innerHTML = '';

				document.getElementById('er2').innerHTML = '';

				document.getElementById('er3').innerHTML = '';

				document.getElementById('er4').innerHTML = '';

				document.getElementById('er5').innerHTML = '';

				document.getElementById('er6').innerHTML = '';

				document.getElementById('er7').innerHTML = '';

				document.getElementById('er8').innerHTML = '';

				document.getElementById('er9').innerHTML = '';

				document.getElementById('er10').innerHTML = '';

				document.getElementById('er11').innerHTML = '';						

				var x = document.getElementById('firstname').value; 

				if(x == '' || x == null)

			    {

				   document.getElementById('er1').innerHTML = '* Enter Firstname';				  

				   document.getElementById('firstname').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z]+$/;  

					if(!numbers.test(x))  

					{

					document.getElementById('er1').innerHTML="* Enter Only Alphabets";

					document.getElementById('firstname').focus();

			    	return false;

			    	}			

				}

				var x = document.getElementById('lastname').value; 

				if(x == '' || x == null)

			    {

				   document.getElementById('er2').innerHTML = '* Enter Lastname';				   

				   document.getElementById('lastname').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z]+$/;  

					if(!numbers.test(x))  

					{

					document.getElementById('er2').innerHTML="* Enter Only Alphabets";					

					document.getElementById('lastname').focus();

			    	return false;

			    	}			

				}

				var x = document.getElementById('namechecks').value; 

				if(x == '' || x == null)

			    {

				   document.getElementById('er3').innerHTML = '* Enter Name on Checks';				  

				   document.getElementById('namechecks').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z]+$/;  

					if(!numbers.test(x))  

					{

					document.getElementById('er3').innerHTML="* Enter Only Alphabets";					

					document.getElementById('namechecks').focus();

			    	return false;

			    	}			

				}				

				var x = document.getElementById('address').value; 

				if(x == '' || x == null)

			    {

				   document.getElementById('er4').innerHTML = '* Enter Address';				  

				   document.getElementById('address').focus();

				 return false;
			    }

				var x = document.getElementById('city').value; 

				if(x == '' || x == null)

			    {

				   document.getElementById('er5').innerHTML = '* Enter City';				  

				   document.getElementById('city').focus();

				  return false;

			    }

				else

				{	


			    	var numbers =  /^[A-Za-z]+$/;  

					if(!numbers.test(x))  

					{

					document.getElementById('er5').innerHTML="* Enter Only Alphabets";					

					document.getElementById('city').focus();

			    	return false;

			    	}			

				}

				var x = document.getElementById('state').value; 

				if(x == '' || x == null)

			    {

				   document.getElementById('er6').innerHTML = '* Enter State';				  

				   document.getElementById('state').focus();

				  return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z]+$/;  

					if(!numbers.test(x))  

					{

					document.getElementById('er6').innerHTML="* Enter Only Alphabets";					

					document.getElementById('state').focus();

			    	return false;

			    	}			

				}				

	           var x = document.getElementById('zip').value;

				if(x=='' || x==null)

				{			

					document.getElementById('er7').innerHTML="* Enter Zip";

					document.getElementById('zip').focus();

			    return false;

				}

				else

				{	

			    	var numbers = /^[0-9]{5}$/; 			

					if(!numbers.test(x))			

					{			

					document.getElementById('er7').innerHTML="* Enter Valid Zip Code";			

					document.getElementById('zip').focus();			

					return false;			

					}			

				}

				var x = document.getElementById('country').value; 

				if(x == '' || x == null)

			    {

				   document.getElementById('er8').innerHTML = '* Enter Country';				  

				   document.getElementById('country').focus();

				   return false;

			    }

				else

				{	

			    	var numbers =  /^[A-Za-z]+$/;  

					if(!numbers.test(x))  

					{

					document.getElementById('er8').innerHTML="* Enter Only Alphabets";					

					document.getElementById('country').focus();

			    	return false;

			    	}			

				}				

				var x = document.getElementById('mphone').value;

				if(x=='' || x==null)

				{			

					document.getElementById('er9').innerHTML="* Enter Mobile Number";

					document.getElementById('mphone').focus();

			    	return false;

				}

				else

				{	

			    	var numbers = /^[0-9]{10}$/; 			

					if(!numbers.test(x))			

					{			

					document.getElementById('er9').innerHTML="* Enter Valid Number";			

					document.getElementById('mphone').focus();			

					return false;			

					}			

				}

				var x = document.getElementById('fax').value;

				if(x=='' || x==null)

				{			

					document.getElementById('er10').innerHTML="* Enter Fax";

					document.getElementById('fax').focus();

			    	return false;

				}

				var x = document.getElementById('email1').value;

                exp=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;

				if(x == null || x == "")				

				{				

				document.getElementById('er11').innerHTML="* Enter Email Address";				

				document.getElementById('email1').focus();				

				return false;				

				}				

				if(!exp.test(x))				

				{				

				document.getElementById('er11').innerHTML="* Invalid Email";				

				document.getElementById('email1').focus();				

				return false;				

				}



                    $scope.errors.splice(0, $scope.errors.length); 

                    $scope.msgs.splice(0, $scope.msgs.length);
					
                    $http.post('webservices/addCustomers.php', {'secrectKey':store.get('secrectKey'),'firstname': $scope.firstname, 'lastname': $scope.lastname, 'term': $scope.term,'namecheck': $scope.namechecks,'businesstype': $scope.businesstype, 'represent': $scope.represent, 'address': $scope.address, 'city': $scope.city, 'state': $scope.state, 'zip': $scope.zip, 'country': $scope.country, 'mphone': $scope.mphone, 'fax': $scope.fax, 'email': $scope.email, 'password': password}

                    ).success(function(data, status, headers, config) {
						console.log(data);
                        if (data.msg != '')

                        {

                            $scope.msgs.push(data.msg);

                        }

                        else

                        {

                            $scope.errors.push(data.error);

                        }

                    }).error(function(data, status) { 

                        $scope.errors.push(status);

                    });

                }           			   

				
			}

			

	  });

   }
  

});// JavaScript Document